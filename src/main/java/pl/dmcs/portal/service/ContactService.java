package pl.dmcs.portal.service;

import javax.persistence.EntityExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Contact;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;

@Service
public class ContactService extends AbstractService<Contact> {

    @Autowired
    UserService userService;

    public PartialList<User> list(String sort, String dir, Integer firstResult, Integer maxResults, Boolean search) {
        if(search != null && search == true) {
            return userService.list(sort, dir, firstResult, maxResults);
        }
        PartialList<User> result = new PartialList<User>();
        Long totalSize = (Long) em.createQuery("select count(*) from Contact").getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select c.contact from Contact c order by " + sort + " " + dir).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    public Contact create(Long contactId) {
        User user = currentUserRetriver.getNotNullCurrentUser();
        if (isContactExist(contactId, user.getId())) {
            throw new EntityExistsException();
        }
        Contact c = new Contact();
        c.setOwner(user);
        c.setContact(userService.read(contactId));
        em.persist(c);
        return c;
    }

    public boolean isContactExist(Long contactId, Long userId) {
        return 0 < (Long) em.createQuery("select count(*) from Contact c where c.owner.id = :ownerId and c.contact.id = :contactId").
                setParameter("contactId", contactId).
                setParameter("ownerId", userId).
                getSingleResult();
    }

    @Override
    protected Class<Contact> getEntityClass() {
        return Contact.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{};
    }
}
