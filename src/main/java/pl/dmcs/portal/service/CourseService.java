package pl.dmcs.portal.service;

import java.util.Map;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Course;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.model.User.UserType;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class CourseService extends AbstractService<Course> {

    public PartialList<Course> list(String sort, String dir, Integer firstResult, Integer maxResults, Boolean assigned) {
        if(assigned == false) {
            return list(sort, dir, firstResult, maxResults);
        }
        PartialList result = new PartialList<Course>();
        User user = currentUserRetriver.getNotNullCurrentUser();
        String countQueryString, queryString;
        if(user.getType() == UserType.LECTURER) {
            countQueryString = "select count(*) from Course c where c.lecturer.id = :userId";
            queryString = "select c from Course c where c.lecturer.id = :userId order by " + sort + " " + dir;
        } else {
            countQueryString = "select count(*) from CourseAssignment ca where ca.user.id = :userId";
            queryString = "select ca.course from CourseAssignment ca where ca.user.id = :userId order by " + sort + " " + dir;
        }
        Long totalSize = (Long) em.createQuery(countQueryString).setParameter("userId", user.getId()).getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery(queryString).
                setParameter("userId", user.getId()).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @Override
    public Course create(Map<String, Object> values) {
        Course c = super.create(values);
        c.setLecturer(currentUserRetriver.getNotNullCurrentUser());
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), c, new String[]{"read","update", "delete"});
        return c;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public Course update(Long id, Map<String, Object> values) {
        return update(read(id), values);
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"name", "description", "unitName", "type"};
    }

    @Override
    protected Class<Course> getEntityClass() {
        return Course.class;
    }
}
