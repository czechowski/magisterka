package pl.dmcs.portal.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.NewsContent;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class NewsFeedService extends AbstractService<NewsContent> {

    @Autowired
    CourseService courseService;
    @Autowired
    NotificationService notificationService;

    public PartialList<NewsContent> list(Integer firstResult, Integer maxResults) {
        User user = currentUserRetriver.getNotNullCurrentUser();
        PartialList result = new PartialList<NewsContent>();
        Long totalSize = (Long) em.createQuery("select count(*) from NewsContent nc where nc.course.id in (select ca.course.id from CourseAssignment ca where ca.user.id = :userId)").
                setParameter("userId", user.getId()).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select nc from NewsContent nc where nc.course.id in (select ca.course.id from CourseAssignment ca where ca.user.id = :userId) order by date DESC").
                setParameter("userId", user.getId()).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    public PartialList<NewsContent> list(Long courseId, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<NewsContent>();
        Long totalSize = (Long) em.createQuery("select count(*) from NewsContent nc where nc.course.id = :courseId").
                setParameter("courseId", courseId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select nc from NewsContent nc where nc.course.id = :courseId order by date DESC").
                setParameter("courseId", courseId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument = "courseId", permission = "update")
    public NewsContent create(Long courseId, Map<String, Object> values) {
        NewsContent nc = super.create(values);
        nc.setCourse(courseService.read(courseId));
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), nc, new String[]{"update", "delete"});
        notificationService.notifyAboutNewsFeedCreation(nc);
        return nc;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public NewsContent update(Long id, Map<String, Object> values) {
        return super.update(read(id), values);
    }

    @Override
    protected Class<NewsContent> getEntityClass() {
        return NewsContent.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"content"};
    }
}
