package pl.dmcs.portal.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Test;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class TestService extends AbstractService<Test> {

    @Autowired
    CourseService courseService;

    public PartialList<Test> list(Long courseId, String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<Test>();
        Long totalSize = (Long) em.createQuery("select count(*) from " + getEntityClass().getSimpleName() + " c where c.course.id = :courseId").
                setParameter("courseId", courseId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select c from " + getEntityClass().getSimpleName() + " c where c.course.id = :courseId order by " + sort + " " + dir).
                setParameter("courseId", courseId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument = "courseId", permission = "update")
    public Test create(Long courseId, Map<String, Object> values) {
        Test t = super.create(values);
        t.setCourse(courseService.read(courseId));
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), t, new String[]{"update", "delete"});
        return t;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public Test update(Long id, Map<String, Object> values) {
        return super.update(read(id), values);
    }

    @Override
    protected Class<Test> getEntityClass() {
        return Test.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"name", "description", "type", "date"};
    }
}
