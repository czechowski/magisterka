package pl.dmcs.portal.service;

import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.DataBinder;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class UserService extends AbstractService<User> {

    public static class DuplicatedEmailException extends RuntimeException {
    }
    @Autowired
    SaltSource saltSource;
    @Autowired
    ShaPasswordEncoder shaPasswordEncoder;

    @Transactional
    @Override
    public User create(Map<String, Object> values) {
        if (checkIfExist((String) values.get("email"))) {
            throw new DuplicatedEmailException();
        }
        User u = new User();
        DataBinder binder = bindingUtil.bind(u, values, getAllowedFields(Operation.CREATE), getSkippedFields(), false);
        String salt = String.valueOf(Math.abs(new Random().nextLong())).substring(0, 8);
        u.setSalt(salt);
        u.setPasswordHash(shaPasswordEncoder.encodePassword((String) values.get("password"), salt));
        binder.validate();
        em.persist(u);
        em.flush();
        securityService.createPermissions(u, u, new String[]{"update", "delete"});
        return u;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public User update(Long id, Map<String, Object> values) {
        return update(read(id), values);
    }

    public User read(String email) {
        return (User) em.createQuery("select u from User u where u.email = :email").setParameter("email", email).getSingleResult();
    }

    public User readCurrentUser() {
        return currentUserRetriver.getNotNullCurrentUser();
    }

    private boolean checkIfExist(String email) {
        return jdbcTemplate.queryForInt("select count(*) from users where email = ?", email) > 0;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        if (operation == Operation.CREATE) {
            return new String[]{"firstName", "lastName", "email", "type", "label", "title", "indexNumber"};
        } else {
            return new String[]{"firstName", "lastName", "label", "title", "indexNumber"};
        }
    }

    @Override
    protected String[] getSkippedFields() {
        return new String[]{"password"};
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }
}
