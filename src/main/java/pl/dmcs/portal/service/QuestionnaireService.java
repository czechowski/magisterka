package pl.dmcs.portal.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Questionnaire;
import pl.dmcs.portal.model.QuestionnaireEntry;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class QuestionnaireService extends AbstractService<Questionnaire> {

    @Autowired
    CourseService courseService;

    public PartialList<Questionnaire> list(Long courseId, String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<Questionnaire>();
        Long totalSize = (Long) em.createQuery("select count(*) from Questionnaire q where q.course.id = :courseId").
                setParameter("courseId", courseId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select q from Questionnaire q left join fetch q.entries left join fetch q.course where q.course.id = :courseId order by q." + sort + " " + dir).
                setParameter("courseId", courseId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument = "courseId", permission = "update")
    public Questionnaire create(Long courseId, Map<String, Object> values) {
        ArrayList<String> entries = null;
        if(values.containsKey("entries")) {
            entries = (ArrayList<String>) values.get("entries");
            values.remove("entries");
        }
        Questionnaire q = super.create(values);
        for(String entry : entries) {
            QuestionnaireEntry qe = new QuestionnaireEntry();
            qe.setAnswer(entry);
            qe.setQuestionnaire(q);
            em.persist(qe);
        }
        q.setCourse(courseService.read(courseId));
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), q, new String[]{"update", "delete"});
        em.flush();
        return q;
    }

    @Transactional(readOnly = true)
    public Questionnaire read(Long courseId, Long id) {
        return (Questionnaire) em.createQuery("select q from Questionnaire q left join fetch q.entries left join fetch q.course where q.id = :id and q.course.id = :courseId").
                setParameter("id", id).
                setParameter("courseId", courseId).
                getSingleResult();
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public Questionnaire update(Long id, Map<String, Object> values) {
        return super.update(read(id), values);
    }

    @Transactional
    @RequiredPermission(argument = "courseId", permission = "read")
    public void answer(Long courseId, Long questionnaireId, Long answerId) {
        em.createQuery("update QuestionnaireEntry qe set qe.total = qe.total + 1 where qe.questionnaire.id = :questionnaireId and qe.id = :answerId").
                setParameter("questionnaireId", questionnaireId).
                setParameter("answerId", answerId).
                executeUpdate();
    }

    @Override
    protected Class<Questionnaire> getEntityClass() {
        return Questionnaire.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"question"};
    }
}
