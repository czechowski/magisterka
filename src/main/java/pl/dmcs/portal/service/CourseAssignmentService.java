package pl.dmcs.portal.service;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Course;
import pl.dmcs.portal.model.CourseAssignment;
import pl.dmcs.portal.model.Group;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class CourseAssignmentService extends AbstractService<CourseAssignment> {

    @Autowired
    CourseService courseService;
    @Autowired
    GroupService groupService;
    @Autowired
    UserService userService;


    public List<CourseAssignment> list(Long courseId, Long groupId, String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<CourseAssignment>();
        Long totalSize = (Long) em.createQuery("select count(*) from CourseAssignment ca where ca.course.id = :courseId and ca.group.id = :groupId").
                setParameter("courseId", courseId).
                setParameter("groupId", groupId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select ca from CourseAssignment ca where ca.course.id = :courseId and ca.group.id = :groupId order by " + sort + " " + dir).
                setParameter("courseId", courseId).
                setParameter("groupId", groupId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    private void createAssignment(Course course, Group group, User user, boolean confirmed) {
        CourseAssignment ca = new CourseAssignment();
        ca.setCourse(course);
        ca.setGroup(group);
        ca.setUser(user);
        ca.setConfirmed(confirmed);
        em.persist(ca);
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument="courseId", permission="update")
    public void confirmAssignment(Long courseId, Long groupId, Long userId) {
        CourseAssignment ca = read(courseId, groupId, userId);
        securityService.createPermissions(userService.read(userId), ca.getCourse(), new String[]{"read"});
        ca.setConfirmed(true);
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument="courseId", permission="update")
    public void createAssignment(Long courseId, Long groupId, Long userId) {
        if(isCourseAssignmentExist(courseId, userId)) {
            throw new EntityExistsException();
        }
        createAssignment(courseService.read(courseId), groupService.read(groupId), userService.read(userId), true);
    }

    @Transactional
    @Secured({"ROLE_STUDENT"})
    public void createAssignment(Long courseId, Long groupId) {
        User user  = currentUserRetriver.getNotNullCurrentUser();
        if(isCourseAssignmentExist(courseId, user.getId())) {
            throw new EntityExistsException();
        }
        createAssignment(courseService.read(courseId), groupService.read(groupId), user, false);
    }

    public List<CourseAssignment> readAssignments(Long courseId) {
        return em.createQuery("select ca from CourseAssignment ca where ca.course.id = :courseId").setParameter("courseId", courseId).getResultList();
    }

    public List<User> readUsers(Long courseId) {
        return em.createQuery("select ca.user from CourseAssignment ca where ca.course.id = :courseId").setParameter("courseId", courseId).getResultList();
    }

    public CourseAssignment read(Long courseId, Long groupId, Long userId) {
        return (CourseAssignment) em.createQuery("select ca from CourseAssignment ca where ca.course.id = :courseId and ca.group.id = :groupId and ca.user.id = :userId").
                setParameter("courseId", courseId).
                setParameter("groupId", groupId).
                setParameter("userId", userId).
                getSingleResult();
    }

    public boolean isCourseAssignmentExist(Long courseId, Long userId) {
        return 0 < (Long) em.createQuery("select count(*) from CourseAssignment ca where ca.course.id = :courseId and ca.user.id = :userId").
                setParameter("courseId", courseId).
                setParameter("userId", userId).
                getSingleResult();
    }

    public boolean isCourseAssignmentConfirmed(Long courseId, Long groupId, Long userId) {
        return 0 < (Long) em.createQuery("select count(*) from CourseAssignment ca where ca.course.id = :courseId and ca.group.id = :groupId and ca.user.id = :userId").
                setParameter("courseId", courseId).
                setParameter("groupId", groupId).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Transactional
    @RequiredPermission(argument = "courseId", permission = "update")
    public CourseAssignment update(Long courseId, Long id, Map<String, Object> values) {
        Long groupId = null;
        if(values.containsKey("group.id")) {
            groupId = Long.parseLong(values.get("group.id").toString());
            values.remove("group.id");
        }
        CourseAssignment ca = super.update(read(id), values);
        if(groupId != null) {
            ca.setGroup(groupService.read(groupId));
        }
        return ca;
    }
    
    @Override
    protected Class<CourseAssignment> getEntityClass() {
        return CourseAssignment.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{};
    }
}
