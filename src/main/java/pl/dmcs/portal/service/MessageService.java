package pl.dmcs.portal.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Message;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class MessageService extends AbstractService<Message> {

    @Autowired
    UserService userService;
    @Autowired
    NotificationService notificationService;

    public PartialList<Message> list(String sort, String dir, Integer firstResult, Integer maxResults, String mode) {
        PartialList result = new PartialList<Message>();
        User user = currentUserRetriver.getNotNullCurrentUser();
        String countQueryString, queryString;
        if (mode.equals("inbox")) {
            countQueryString = "select count(*) from Message m where m.owner.id = :userId and m.receiver.id = :userId";
            queryString = "select m from Message m where m.owner.id = :userId and m.receiver.id = :userId order by " + sort + " " + dir;
        } else {
            countQueryString = "select count(*) from Message m where m.owner.id = :userId and m.sender.id = :userId";
            queryString = "select m from Message m where m.owner.id = :userId and m.sender.id = :userId order by " + sort + " " + dir;
        }
        Long totalSize = (Long) em.createQuery(countQueryString).setParameter("userId", user.getId()).getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery(queryString).
                setParameter("userId", user.getId()).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @Override
    public Message create(Map<String, Object> values) {
        User sender = currentUserRetriver.getNotNullCurrentUser();
        User receiver = userService.read((String) values.get("receiver"));
        Message senderMessage = super.create(values);
        senderMessage.setOwner(sender);
        senderMessage.setSender(sender);
        senderMessage.setReceiver(receiver);
        senderMessage.setRead(true);
        securityService.createPermissions(sender, senderMessage, new String[]{"update", "delete"});
        if (!sender.equals(receiver)) {
            Message receiverMessage = super.create(values);
            receiverMessage.setOwner(receiver);
            receiverMessage.setSender(sender);
            receiverMessage.setReceiver(receiver);
            securityService.createPermissions(receiver, receiverMessage, new String[]{"update", "delete"});
            notificationService.notifyAboutReceivedMessage(receiverMessage);
        }
        return senderMessage;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public Message update(Long id, Map<String, Object> values) {
        return update(read(id), values);
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "delete")
    @Override
    public void delete(Long id) {
        Message m = read(id);
        securityService.removePermissions(currentUserRetriver.getNotNullCurrentUser(), m, new String[]{"update", "delete"});
        delete(m);
    }

    @Override
    protected Class<Message> getEntityClass() {
        return Message.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        if (operation == Operation.CREATE) {
            return new String[]{"text", "subject"};
        } else {
            return new String[]{"read"};
        }
    }

    @Override
    protected String[] getSkippedFields() {
        return new String[]{"receiver"};
    }
}
