package pl.dmcs.portal.service;

import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.TestResult;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class TestResultService extends AbstractService<TestResult> {

    @Autowired
    TestService testService;
    @Autowired
    CourseAssignmentService courseAssignmnetService;

    public ArrayList<Object[]> list(Long courseId, Long groupId) {
        return (ArrayList<Object[]>) em.createQuery("select distinct ca, tr from TestResult tr right join tr.assignment ca where ca.course.id = :courseId and ca.group.id = :groupId order by ca.user.lastName ASC").
                setParameter("courseId", courseId).
                setParameter("groupId", groupId).
                getResultList();
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument = "testId", permission = "update")
    public TestResult create(Long testId, Map<String, Object> values) {
        Long assignmentId = Long.parseLong(values.get("assignment").toString());
        values.remove("assignment");
        TestResult t = super.create(values);
        t.setTest(testService.read(testId));
        t.setAssignment(courseAssignmnetService.read(assignmentId));
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), t, new String[]{"update", "delete"});
        return t;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public TestResult update(Long id, Map<String, Object> values) {
        return super.update(read(id), values);
    }

    @Override
    protected Class<TestResult> getEntityClass() {
        return TestResult.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"result", "date"};
    }

    @Override
    protected String[] getSkippedFields() {
        return new String[]{"student", "group"};
    }
}
