package pl.dmcs.portal.service;

import java.io.IOException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.dmcs.portal.model.CourseFile;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class CourseFileService extends AbstractService<CourseFile> {

    @Autowired
    CourseService courseService;

    public PartialList<CourseFile> list(String sort, String dir, Integer firstResult, Integer maxResults, Long courseId) {
        PartialList result = new PartialList<CourseFile>();
        Long totalSize = (Long) em.createQuery("SELECT count(*) FROM CourseFile cf WHERE cf.course.id = :courseId").
                setParameter("courseId", courseId).getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("SELECT cf FROM CourseFile cf WHERE cf.course.id = :courseId order by " + sort + " " + dir).
                setParameter("courseId", courseId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @RequiredPermission(argument = "courseId", permission = "read")
    public CourseFile download(Long courseId, Long id) {
        return (CourseFile) em.createQuery("SELECT cf FROM CourseFile cf WHERE cf.id = :id").
                setParameter("id", id).getSingleResult();
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument = "courseId", permission = "update")
    public CourseFile create(Long courseId, MultipartFile file, String description) throws IOException {
        if (file.getSize() > 16777216) {
            throw new IllegalArgumentException("File " + file.getOriginalFilename() + " size exceeded");
        }
        CourseFile cf = new CourseFile();
        cf.setCourse(courseService.read(courseId));
        cf.setName(file.getOriginalFilename());
        cf.setSize(file.getSize());
        cf.setType(file.getContentType());
        cf.setBytes(file.getBytes());
        if (description != null) {
            cf.setDescription(description);
        }
        em.persist(cf);
        em.flush();
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), cf, new String[]{"update", "delete"});
        return cf;
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "update")
    @Override
    public CourseFile update(Long id, Map<String, Object> values) {
        return update(read(id), values);
    }

    @Transactional
    @RequiredPermission(argument = "id", permission = "delete")
    @Override
    public void delete(Long id) {
        CourseFile cf = read(id);
        securityService.removePermissions(currentUserRetriver.getNotNullCurrentUser(), cf, new String[]{"update", "delete"});
        delete(cf);
    }

    @Override
    protected Class<CourseFile> getEntityClass() {
        return CourseFile.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        if (operation == Operation.UPDATE) {
            return new String[]{"description"};
        } else {
            return new String[]{};
        }
    }
}
