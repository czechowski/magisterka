package pl.dmcs.portal.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.Group;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;
import pl.dmcs.portal.security.RequiredPermission;

@Service
public class GroupService extends AbstractService<Group> {

    @Autowired
    CourseService courseService;

    public PartialList<Group> list(Long courseId, String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<Group>();
        Long totalSize = (Long) em.createQuery("select count(*) from " + getEntityClass().getSimpleName() + " c where c.course.id = :courseId").
                setParameter("courseId", courseId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select c from " + getEntityClass().getSimpleName() + " c where c.course.id = :courseId order by " + sort + " " + dir).
                setParameter("courseId", courseId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    @Secured({"ROLE_LECTURER"})
    @RequiredPermission(argument="courseId", permission="update")
    public Group create(Long courseId, Map<String, Object> values) {
        Group g = super.create(values);
        g.setCourse(courseService.read(courseId));
        securityService.createPermissions(currentUserRetriver.getNotNullCurrentUser(), g, new String[]{"update", "delete"});
        return g;
    }

    @Transactional
    @RequiredPermission(argument="id", permission="update")
    @Override
    public Group update(Long id, Map<String, Object> values) {
        return super.update(read(id), values);
    }

    @Override
    protected Class<Group> getEntityClass() {
        return Group.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"name", "date", "room", "year"};
    }
}
