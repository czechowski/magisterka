package pl.dmcs.portal.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.portal.model.Message;
import pl.dmcs.portal.model.NewsContent;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.MailSenderUtil;

@Service
public class NotificationService {

    @Autowired
    MailSenderUtil mailSenderUtil;
    @Autowired
    CourseAssignmentService courseAssignmentService;

    public void notifyAboutReceivedMessage(Message message) {
        String subject = "Nowa wiadomosc na Portalu Studenckim";
        String text = "Nadawca: " + message.getSender().getLabel() + "<br/>Temat: " + message.getSubject() + "<br/><br/><hr/>" + message.getText();
        mailSenderUtil.send(message.getReceiver(), subject, text);
    }

    public void notifyAboutNewsFeedCreation(NewsContent nc) {
        String subject = "Nowa ogloszenie na Portalu Studenckim";
        String text = "Przedmiot: " + nc.getCourse().getName() + "<br/>Prowadzacy: " + nc.getCourse().getLecturer().getLabel() + "<br/><br/><hr/>" + nc.getContent();
        List<User> recipients = courseAssignmentService.readUsers(nc.getCourse().getId());
        if(recipients.size() > 0) {
            mailSenderUtil.send(recipients, subject, text);
        }
    }
}
