package pl.dmcs.portal.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.ForumThread;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.support.AbstractService;
import pl.dmcs.portal.service.support.PartialList;

@Service
public class ForumService extends AbstractService<ForumThread> {

    @Autowired
    CourseService courseService;

    @Override
    public PartialList<ForumThread> list(String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<ForumThread>();
        Long totalSize = (Long) em.createQuery("select count(*) from ForumThread t where t.course = null and t.parent = null").
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select t from ForumThread t where t.course = null and t.parent = null order by " + sort + " " + dir).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    public PartialList<ForumThread> list(Long parentId, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<ForumThread>();
        Long totalSize = (Long) em.createQuery("select count(*) from ForumThread t where t.course = null and t.parent.id = :parentId").
                setParameter("parentId", parentId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select t from ForumThread t where t.course = null and (t.parent.id = :parentId or t.id = :parentId) order by date ASC").
                setParameter("parentId", parentId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    public PartialList<ForumThread> listByCourse(Long courseId, String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<ForumThread>();
        Long totalSize = (Long) em.createQuery("select count(*) from ForumThread t where t.course.id = :courseId and t.parent = null").
                setParameter("courseId", courseId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select t from ForumThread t where t.course.id = :courseId and t.parent = null order by " + sort + " " + dir).
                setParameter("courseId", courseId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    public PartialList<ForumThread> listByCourse(Long courseId, Long parentId, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<ForumThread>();
        Long totalSize = (Long) em.createQuery("select count(*) from ForumThread t where t.course.id = :courseId and t.parent.id = :parentId").
                setParameter("courseId", courseId).
                setParameter("parentId", parentId).
                getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select t from ForumThread t where t.course.id = :courseId and (t.parent.id = :parentId or t.id = :parentId) order by date ASC").
                setParameter("courseId", courseId).
                setParameter("parentId", parentId).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    public ForumThread create(Long parentId, Long courseId, Map<String, Object> values) {
        User user = currentUserRetriver.getNotNullCurrentUser();
        ForumThread t = super.create(values);
        t.setUser(user);
        if (courseId != null) {
            t.setCourse(courseService.read(courseId));
        }
        if (parentId != null) {
            ForumThread parent = read(parentId);
            t.setParent(parent);
            parent.setReplies(parent.getReplies() + 1);
        } else {
            t.setReplies(0);
        }
        return t;
    }

    @Override
    protected Class<ForumThread> getEntityClass() {
        return ForumThread.class;
    }

    @Override
    protected String[] getAllowedFields(Operation operation) {
        return new String[]{"text", "subject"};
    }

    @Override
    protected String[] getSkippedFields() {
        return new String[]{"parent"};
    }
}
