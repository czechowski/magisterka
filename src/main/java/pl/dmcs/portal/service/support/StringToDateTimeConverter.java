package pl.dmcs.portal.service.support;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.core.convert.converter.Converter;

public class StringToDateTimeConverter implements Converter<String, DateTime> {

    DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
    
    @Override
    public DateTime convert(String source) {
        return parser.parseDateTime(source);
    }

}
