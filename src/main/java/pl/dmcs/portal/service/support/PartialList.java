package pl.dmcs.portal.service.support;

import java.util.ArrayList;


public class PartialList<E> extends ArrayList<E> {

    private Long totalSize = 0l;

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

}
