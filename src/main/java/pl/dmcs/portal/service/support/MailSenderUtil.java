package pl.dmcs.portal.service.support;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import pl.dmcs.portal.model.User;

@Component
public class MailSenderUtil {

    static Logger log = LoggerFactory.getLogger(MailSenderUtil.class);
    @Autowired
    JavaMailSender mailSender;
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    public void send(final List<User> recipients, final String subject, final String text) {
        threadPoolTaskScheduler.execute(new Runnable() {

            @Override
            public void run() {
                try {
                    final InternetAddress[] recipientsInternetAddresses = new InternetAddress[recipients.size()];

                    for (int iter = 0; iter < recipients.size(); iter++) {
                        recipientsInternetAddresses[iter] = new InternetAddress(recipients.get(iter).getEmail(), recipients.get(iter).getLabel());
                    }

                    MimeMessagePreparator preparator = new MimeMessagePreparator() {

                        @Override
                        public void prepare(MimeMessage mail) throws Exception {
                            mail.setFrom(new InternetAddress("portal.localhost@gmail.com", "Portal Studencki"));
                            mail.setSubject(subject, "UTF-8");
                            mail.setText(text, "UTF-8");
                            mail.setRecipients(RecipientType.TO, recipientsInternetAddresses);
                            mail.addHeader("Content-Type", "text/html");
                        }
                    };
                    mailSender.send(preparator);
                } catch (MailException ex) {
                    log.error(ex.getMessage(), ex);
                } catch (UnsupportedEncodingException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        });
    }

    public void send(User recipient, String subject, String text) {
        send(Arrays.asList(recipient), subject, text);
    }
}
