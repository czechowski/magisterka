package pl.dmcs.portal.service.support;

import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.EntityBase;
import pl.dmcs.portal.security.CurrentUserRetriever;
import pl.dmcs.portal.security.SecurityService;

public abstract class AbstractService<E extends EntityBase> {

    protected enum Operation {

        CREATE, UPDATE
    };
    @PersistenceContext
    protected EntityManager em;
    @Autowired
    protected BindingUtil bindingUtil;
    @Autowired
    protected CurrentUserRetriever currentUserRetriver;
    @Autowired
    protected JdbcTemplate jdbcTemplate;
    @Autowired
    protected SecurityService securityService;

    public PartialList<E> list(String sort, String dir, Integer firstResult, Integer maxResults) {
        PartialList result = new PartialList<E>();
        Long totalSize = (Long) em.createQuery("select count(*) from " + getEntityClass().getSimpleName()).getSingleResult();
        result.setTotalSize(totalSize);
        result.addAll(em.createQuery("select c from " + getEntityClass().getSimpleName() + " c order by " + sort + " " + dir).
                setFirstResult(firstResult).
                setMaxResults(maxResults).
                getResultList());
        return result;
    }

    @Transactional
    public E create(Map<String, Object> values) {
        E entity;
        try {
            entity = getEntityClass().newInstance();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        bindingUtil.bind(entity, values, getAllowedFields(Operation.CREATE), getSkippedFields());
        em.persist(entity);
        return entity;
    }

    @Transactional(readOnly = true)
    public E read(Long id) {
        return (E) em.createQuery("select o from " + getEntityClass().getSimpleName() + " o where o.id = :id").
                setParameter("id", id).
                getSingleResult();
    }

    @Transactional
    public E update(Long id, Map<String, Object> values) {
        return update(read(id), values);
    }

    @Transactional
    public E update(E entity, Map<String, Object> values) {
        bindingUtil.bind(entity, values, getAllowedFields(Operation.UPDATE), getSkippedFields());
        return entity;
    }

    @Transactional
    public void delete(Long id) {
        delete(read(id));
    }

    @Transactional
    public void delete(E entity) {
        em.remove(entity);
    }

    protected abstract Class<E> getEntityClass();

    protected abstract String[] getAllowedFields(Operation operation);

    protected String[] getSkippedFields() {
        return new String[0];
    }
}
