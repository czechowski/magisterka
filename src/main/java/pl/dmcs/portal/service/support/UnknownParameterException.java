package pl.dmcs.portal.service.support;

import java.util.Arrays;

public class UnknownParameterException extends RuntimeException {

    String[] parameterNames;

    UnknownParameterException(String[] parameterNames) {
        super("Illegal parameters: " + Arrays.toString(parameterNames));
        this.parameterNames = parameterNames;
    }

    UnknownParameterException(String parameterName) {
        super("Illegal parameter: " + parameterName);
        this.parameterNames = new String[] {parameterName};
    }

    public String[] getParameterNames() {
        return parameterNames;
    }


}
