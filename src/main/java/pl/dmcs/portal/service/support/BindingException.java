package pl.dmcs.portal.service.support;

import java.util.List;
import org.springframework.validation.ObjectError;

public class BindingException extends RuntimeException {

    List<ObjectError> errors;

    BindingException(List<ObjectError> errors) {
        super("Binding errors occured: " + errors.toString());
        this.errors = errors;
    }

    public List<ObjectError> getErrors() {
        return errors;
    }
}
