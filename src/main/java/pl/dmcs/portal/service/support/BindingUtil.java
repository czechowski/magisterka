package pl.dmcs.portal.service.support;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyAccessorUtils;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.DefaultBindingErrorProcessor;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

@Component
public class BindingUtil {

    @Autowired
    protected ConversionService conversionService;
    @Autowired
    protected SpringValidatorAdapter validator;

    /**
     *
     * @param target
     * @param newValues
     * @param allowedFields
     * @param skipFields  an array of fields which should be "hidden" for binder
     */
    public DataBinder bind(Object target, Map<String, Object> newValues, String[] allowedFields, String[] skipFields) {
        return bind(target, newValues, allowedFields, skipFields, true);
    }

    public DataBinder bind(Object target, Map<String, Object> newValues, String[] allowedFields, String[] skipFields, boolean validate) {
        Map<String, Object> skippedValues = null;
        if (skipFields != null && skipFields.length > 0) {
            skippedValues = new HashMap<String, Object>();
            for (String skipField : skipFields) {
                if (newValues.containsKey(skipField)) {
                    skippedValues.put(skipField, newValues.remove(skipField));
                }
            }
        }
        DataBinder binder = new OrderedDataBinder(target);
        binder.setConversionService(conversionService);
        binder.setValidator(validator);
        binder.setAllowedFields(allowedFields);
        binder.setBindingErrorProcessor(new DefaultBindingErrorProcessor());
        binder.bind(new MutablePropertyValues(newValues));
        if (validate && !binder.getBindingResult().hasErrors()) {
            binder.validate();
        }
        BindingResult bindingResult = binder.getBindingResult();
        if (bindingResult.hasErrors()) {
            throw new BindingException(bindingResult.getAllErrors());
        }
        if (bindingResult.getSuppressedFields().length > 0) {
            throw new UnknownParameterException(bindingResult.getSuppressedFields());
        }
        if (skippedValues != null) {
            //revert skipped fields
            for (String key : skippedValues.keySet()) {
                newValues.put(key, skippedValues.get(key));
            }
        }
        return binder;
    }

    public void validate(Object target) {
        bind(target, Collections.<String, Object>emptyMap(), new String[]{"*"}, null, true);
    }

    class OrderedDataBinder extends DataBinder {

        public OrderedDataBinder(Object target, String objectName) {
            super(target, objectName);
        }

        public OrderedDataBinder(Object target) {
            super(target);
        }

        @Override
        protected void checkAllowedFields(MutablePropertyValues mpvs) {
            super.checkAllowedFields(mpvs);
            String[] allowedFields = this.getAllowedFields();
            final Map<String, Integer> allowedFieldsIndex = new HashMap<String, Integer>();
            for (int i = 0; i < this.getAllowedFields().length; i++) {
                allowedFieldsIndex.put(allowedFields[i], i);
            }
            List<PropertyValue> propertyValues = mpvs.getPropertyValueList();
            Collections.sort(propertyValues, new Comparator<PropertyValue>() {

                @Override
                public int compare(PropertyValue pv1, PropertyValue pv2) {
                    String field1 = PropertyAccessorUtils.canonicalPropertyName(pv1.getName());
                    String field2 = PropertyAccessorUtils.canonicalPropertyName(pv2.getName());
                    Integer field1Order = allowedFieldsIndex.get(field1);
                    Integer field2Order = allowedFieldsIndex.get(field2);
                    if (field1Order == null) {
                        field1Order = findWildcardPosition(field1, allowedFieldsIndex);
                    }
                    if (field2Order == null) {
                        field2Order = findWildcardPosition(field2, allowedFieldsIndex);
                    }
                    return field1Order - field2Order;
                }
            });
        }

        private Integer findWildcardPosition(String field, Map<String, Integer> allowedFieldsIndex) {
            field = field.substring(0, field.lastIndexOf(".")) + ".*";
            if (allowedFieldsIndex.containsKey(field)) {
                return allowedFieldsIndex.get(field);
            } else {
                return findWildcardPosition(field, allowedFieldsIndex);
            }
        }
    }
}
