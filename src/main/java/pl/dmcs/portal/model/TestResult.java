package pl.dmcs.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "test_results")
public class TestResult extends EntityBase {

    @ManyToOne
    private Test test;
    @ManyToOne
    private CourseAssignment assignment;
    @Size(max = 20)
    @Column(name = "_result")
    private String result;
    @Column(name = "_date")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date;

    public CourseAssignment getAssignment() {
        return assignment;
    }

    public void setAssignment(CourseAssignment assignment) {
        this.assignment = assignment;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
