package pl.dmcs.portal.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "questionnaires")
public class Questionnaire extends EntityBase {

    private String question;
    @ManyToOne(fetch = FetchType.LAZY)
    private Course course;
    @OneToMany(mappedBy = "questionnaire", cascade = CascadeType.ALL)
    private List<QuestionnaireEntry> entries;
    @Column(name = "_date")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date = new DateTime();

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public List<QuestionnaireEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<QuestionnaireEntry> entries) {
        this.entries = entries;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
