package pl.dmcs.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "groups")
public class Group extends EntityBase {

    @Size(max = 30)
    private String name;
    @Size(max = 20)
    @Column(name = "_date")
    private String date;
    @Column(name = "_year")
    @Size(max = 10)
    private String year;
    @Size(max = 20)
    private String room;
    @ManyToOne(fetch = FetchType.LAZY)
    private Course course;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
