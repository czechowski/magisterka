package pl.dmcs.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import javax.validation.constraints.Size;
import org.joda.time.DateTime;

@Entity
@Table(name = "news_feed")
public class NewsContent extends EntityBase {

    @ManyToOne
    private Course course;
    @Column(name = "_date")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date = new DateTime();
    @Size(max = 1000)
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

}
