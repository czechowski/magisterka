package pl.dmcs.portal.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "courses")
public class Course extends EntityBase {

    public enum CourseType {
        LECTURE, EXERCISES, LABORATORY
    }

    @NotNull
    @Size(max = 100)
    private String name;
    @Size(max = 100)
    private String unitName;
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private CourseType type;
    @Size(max = 10000)
    private String description;
    @ManyToOne
    private User lecturer;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getLecturer() {
        return lecturer;
    }

    public void setLecturer(User lecturer) {
        this.lecturer = lecturer;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public CourseType getType() {
        return type;
    }

    public void setType(CourseType type) {
        this.type = type;
    }
}
