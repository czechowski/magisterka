package pl.dmcs.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "forum_threads")
public class ForumThread extends EntityBase {

    @Size(max = 100)
    private String subject;
    @Size(max = 5000)
    private String text;
    @ManyToOne
    private ForumThread parent;
    @ManyToOne
    private User user;
    @ManyToOne
    private Course course;
    @Column(name = "_date")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date = new DateTime();
    private Integer replies;
    @Column(name = "last_post_info")
    private String lastPostInfo;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getLastPostInfo() {
        return lastPostInfo;
    }

    public void setLastPostInfo(String lastPostInfo) {
        this.lastPostInfo = lastPostInfo;
    }

    public ForumThread getParent() {
        return parent;
    }

    public void setParent(ForumThread parent) {
        this.parent = parent;
    }

    public Integer getReplies() {
        return replies;
    }

    public void setReplies(Integer replies) {
        this.replies = replies;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
