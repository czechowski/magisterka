package pl.dmcs.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "messages")
public class Message extends EntityBase {

    @ManyToOne
    private User sender;
    @ManyToOne
    private User receiver;
    @ManyToOne
    private User owner;
    @Size(max = 3000)
    private String text;
    @Size(max = 100)
    private String subject;
    @Column(name = "_date")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date = new DateTime();
    @Column(name = "_read")
    private boolean read = false;

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
