package pl.dmcs.portal.model;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.TableGenerator;

@MappedSuperclass
public class EntityBase implements Serializable {

    @Id
    @TableGenerator(name = "idGenerator", table = "id_generator_table", pkColumnName = "name", valueColumnName = "value", pkColumnValue = "id")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "idGenerator")
    private Long id;

    public Long getId() {
        return id;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityBase other = (EntityBase) obj;
        if (this.id == null || !this.id.equals(other.id)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
