package pl.dmcs.portal.security;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredPermission {
    String argument();
    String permission();
}
