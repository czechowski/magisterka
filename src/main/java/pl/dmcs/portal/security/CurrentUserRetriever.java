package pl.dmcs.portal.security;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.dmcs.portal.model.User;

@Component
public class CurrentUserRetriever {

    public static CurrentUserRetriever getInstance() {
        return new CurrentUserRetriever();
    }

    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().
                getAuthentication();
        return getUserFromAuthenticationToken(auth);
    }

    public User getNotNullCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().
                getAuthentication();
        if (auth == null || !(auth.getPrincipal() instanceof User)) {
            throw new AuthenticationCredentialsNotFoundException("Requires authentication");
        }
        User user = (User) auth.getPrincipal();
        return user;
    }

    public boolean isCurrentUser(User user) {
        return this.getCurrentUser().getId().equals(user.getId());
    }

    public static User getUserFromAuthenticationToken(Authentication auth) {
        if (auth == null) {
            return null;
        }
        if (auth.getPrincipal() instanceof User) {
            User user = (User) auth.getPrincipal();
            return user;
        } else {
            return null;
        }
    }
}
