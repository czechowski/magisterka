package pl.dmcs.portal.security;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.portal.model.EntityBase;
import pl.dmcs.portal.model.Permission;
import pl.dmcs.portal.model.User;

@Service
public class SecurityService {

    @PersistenceContext
    EntityManager em;

    public void checkAuthorization(Long userId, Long entityId, String permission) {
        String query = "select count(*) from Permission p where p.entityId = :entityId and p.userId = :userId and p.permission = :permission";
        Long count = em.createQuery(query, Long.class).
                setParameter("entityId", entityId).
                setParameter("userId", userId).
                setParameter("permission", permission).
                getSingleResult();
        if (count == 0) {
            throw new AccessDeniedException("Access denied for user_id = "
                    + userId + ", entity_id = "
                    + entityId + ", permission = "
                    + permission);
        }
    }

    public void checkAuthorization(User user, EntityBase entity, String permission) {
        checkAuthorization(user.getId(), entity.getId(), permission);
    }

    @Transactional
    public void createPermission(Long userId, Long entityId, String permission) {
        Permission p = new Permission();
        p.setEntityId(entityId);
        p.setUserId(userId);
        p.setPermission(permission);
        em.persist(p);
    }

    @Transactional
    public void createPermission(User user, EntityBase entity, String permission) {
        createPermission(user.getId(), entity.getId(), permission);
    }

    @Transactional
    public void createPermissions(User user, EntityBase entity, String[] permissions) {
        for (String permission : permissions) {
            createPermission(user, entity, permission);
        }
    }

    @Transactional
    public void removePermission(Long userId, Long entityId, String permission) {
        String query = "delete from Permission p where p.entityId = :entityId and p.userId = :userId and p.permission = :permission";
        em.createQuery(query).setParameter("entityId", entityId).
                setParameter("userId", userId).
                setParameter("permission", permission).
                executeUpdate();
    }

    @Transactional
    public void removePermission(User user, EntityBase entity, String permission) {
        removePermission(user.getId(), entity.getId(), permission);
    }

    @Transactional
    public void removePermissions(User user, EntityBase entity, String[] permissions) {
        for (String permission : permissions) {
            removePermission(user, entity, permission);
        }
    }
}
