package pl.dmcs.portal.security;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.EmptyResultDataAccessException;
import pl.dmcs.portal.model.User;

public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * @param username
     * @return
     * @throws UsernameNotFoundException
     * @throws DataAccessException
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException, DataAccessException {

        Long userId = null;

        try {
            userId = jdbcTemplate.queryForLong("select id from users where email = ?", username);
            assert userId != null;
        } catch (EmptyResultDataAccessException e) {
            throw new UsernameNotFoundException(username);
        }

        User u = em.find(User.class, userId);
        if (u == null) {
            throw new UsernameNotFoundException(username);
        }
        return u;

    }
}
