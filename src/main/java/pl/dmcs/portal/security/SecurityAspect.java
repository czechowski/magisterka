package pl.dmcs.portal.security;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class SecurityAspect {

    @Autowired
    private SecurityService securityService;
    @Autowired
    private CurrentUserRetriever currentUserRetriver;

    @Before("@annotation(rp)")
    public void beforeMethodAdvice(JoinPoint jp, RequiredPermission rp) {
        String argument = rp.argument();
        String permission = rp.permission();

        String[] paramNames = ((CodeSignature) jp.getStaticPart().
                getSignature()).getParameterNames();
        Object[] args = jp.getArgs();
        for (int i = 0; i < paramNames.length; i++) {
            if (argument.equals(paramNames[i]) && args[i] instanceof Long) {
                Long entityId = (Long) args[i];
                securityService.checkAuthorization(currentUserRetriver.getNotNullCurrentUser().getId(), entityId, permission);
            }
        }
    }
}
