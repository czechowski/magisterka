package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.ForumThread;
import pl.dmcs.portal.service.ForumService;

@Controller
public class ForumController extends AbstractController<ForumThread> {

    @Autowired
    ForumService service;

    @RequestMapping(method = RequestMethod.GET, value = "/forum")
    public HttpResponse list(
            @RequestParam(defaultValue = "date") String sort,
            @RequestParam(defaultValue = "DESC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(sort, dir, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/forum/{parentId}/thread")
    public HttpResponse list(
            @PathVariable Long parentId,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(parentId, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/forum")
    public HttpResponse create(@RequestBody Map<String, Object> values) {
        ForumThread t = service.create(null, null, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(t), true)).setHeader("Location", "svc/forum/" + t.getId());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/forum/{parentId}/thread")
    public HttpResponse createReply(@PathVariable Long parentId, @RequestBody Map<String, Object> values) {
        ForumThread t = service.create(parentId, null, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(t), true)).setHeader("Location", "svc/forum/" + t.getId());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/forum/{id}")
    public HttpResponse read(@PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/courses/{courseId}/forum")
    public HttpResponse listByCourse(
            @PathVariable Long courseId,
            @RequestParam(defaultValue = "date") String sort,
            @RequestParam(defaultValue = "DESC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.listByCourse(courseId, sort, dir, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/courses/{courseId}/forum/{parentId}/thread")
    public HttpResponse listByCourse(
            @PathVariable Long courseId,
            @PathVariable Long parentId,
            @RequestParam(defaultValue = "date") String sort,
            @RequestParam(defaultValue = "DESC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.listByCourse(courseId, parentId, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/courses/{courseId}/forum")
    public HttpResponse createByCourse(@PathVariable Long courseId, @RequestBody Map<String, Object> values) {
        ForumThread t = service.create(null, courseId, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(t), true)).setHeader("Location", "svc/courses/" + courseId + "/forum/" + t.getId());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/courses/{courseId}/forum/{parentId}/thread")
    public HttpResponse createByCourse(@PathVariable Long parentId, @PathVariable Long courseId, @RequestBody Map<String, Object> values) {
        ForumThread t = service.create(parentId, courseId, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(t), true)).setHeader("Location", "svc/courses/" + courseId + "/forum/" + parentId + "/thread/" + t.getId());
    }

    @Override
    public JSONObject getJsonRepresentation(ForumThread t) {
        JSONObject result = new JSONObject().element("id", t.getId()).
                element("subject", t.getSubject()).
                element("text", t.getText()).
                element("date", dateTimeFormatter.print(t.getDate())).
                element("replies", t.getReplies()).
                element("lastPostInfo", t.getLastPostInfo());
        result.element("user", new JSONObject().element("id", t.getUser().getId()).element("label", t.getUser().getLabel()));
        return result;
    }
}
