package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.Course;
import pl.dmcs.portal.service.CourseService;

@Controller
@RequestMapping("/courses")
public class CourseController extends AbstractController<Course> {

    @Autowired
    CourseService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse create(@RequestBody Map<String, Object> values) {
        Course course = service.create(values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(course), true)).setHeader("Location", "svc/courses/" + course.getId());
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @RequestParam(defaultValue = "name") String sort,
            @RequestParam(defaultValue = "ASC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            @RequestParam(defaultValue = "false") Boolean search) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(sort, dir, start, limit, !search), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long id, @RequestBody Map<String, Object> values) {
        Course course = service.update(id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(course), true));
    }

    @Override
    public JSONObject getJsonRepresentation(Course course) {
        return new JSONObject().element("id", course.getId()).
                element("name", course.getName()).
                element("unitName", course.getUnitName()).
                element("type", course.getType().name()).
                element("description", course.getDescription()).
                element("lecturer", new JSONObject().element("id", course.getLecturer().getId()).
                element("label", course.getLecturer().getLabel()));
    }
}
