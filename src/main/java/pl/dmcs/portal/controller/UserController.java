package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.UserService;

@Controller
@RequestMapping("/users")
public class UserController extends AbstractController<User> {

    @Autowired
    UserService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse create(@RequestBody Map<String, Object> values) {
        User user = service.create(values);
        return new JsonResponse(201, getJsonRepresentation(user)).setHeader("Location", "svc/users/" + user.getId());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/current")
    public HttpResponse read() {
        User user = service.readCurrentUser();
        return new JsonResponse(200, getJsonRepresentation(user));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long id, @RequestBody Map<String, Object> values) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.update(id, values)), true));
    }

    @Override
    public JSONObject getJsonRepresentation(User user) {
        return new JSONObject().element("id", user.getId()).
                element("email", user.getEmail()).
                element("firstName", user.getFirstName()).
                element("lastName", user.getLastName()).
                element("label", user.getLabel()).
                element("title", user.getTitle()).
                element("indexNumber", user.getIndexNumber()).
                element("type", user.getType());
    }
}
