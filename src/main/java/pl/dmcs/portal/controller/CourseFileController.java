package pl.dmcs.portal.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.CourseFile;
import pl.dmcs.portal.service.CourseFileService;

@Controller
@RequestMapping("/courses/{courseId}/files")
public class CourseFileController extends AbstractController<CourseFile> {

    @Autowired
    CourseFileService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse fileUpload(@PathVariable Long courseId, @RequestParam final MultipartFile file, @RequestParam String description,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        service.create(courseId, file, description);
        return new JsonResponse(HttpServletResponse.SC_OK, new JSONObject().element("success", true)).setContentType("text/html");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/download")
    public void fileDownload(@PathVariable Long courseId, @PathVariable Long id, HttpServletResponse response) throws IOException {
        CourseFile cf = service.download(courseId, id);
        response.setStatus(200);
        response.setContentLength(cf.getBytes().length);
        response.setHeader("Content-Disposition","attachment; filename=\"" + cf.getName() +"\"");
        response.setContentType(cf.getType());
        response.getOutputStream().write(cf.getBytes());
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @PathVariable Long courseId,
            @RequestParam(defaultValue = "name") String sort,
            @RequestParam(defaultValue = "ASC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(sort, dir, start, limit, courseId), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long courseId, @PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long courseId, @PathVariable Long id, @RequestBody Map<String, Object> values) {
        CourseFile cf = service.update(id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(cf), true));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public HttpResponse delete(@PathVariable Long courseId, @PathVariable Long id) {
        service.delete(id);
        return new JsonResponse(HttpServletResponse.SC_OK);
    }

    @Override
    public JSONObject getJsonRepresentation(CourseFile t) {
        return new JSONObject().element("id", t.getId()).
                element("name", t.getName()).
                element("size", t.getSize()).
                element("description", t.getDescription());
    }

}
