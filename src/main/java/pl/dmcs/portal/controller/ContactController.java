package pl.dmcs.portal.controller;

import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.User;
import pl.dmcs.portal.service.ContactService;

@Controller
@RequestMapping("/contacts")
public class ContactController extends AbstractController<User> {

    @Autowired
    ContactService service;

    @RequestMapping(method = RequestMethod.POST, value = "/{contactId}")
    public HttpResponse create(@PathVariable Long contactId) {
        service.create(contactId);
        return new JsonResponse(HttpServletResponse.SC_CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @RequestParam(defaultValue = "lastName") String sort,
            @RequestParam(defaultValue = "ASC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            @RequestParam(required = false) Boolean search) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(sort, dir, start, limit, search), null));
    }

    @Override
    public JSONObject getJsonRepresentation(User user) {
        return new JSONObject().element("id", user.getId()).
                element("email", user.getEmail()).
                element("firstName", user.getFirstName()).
                element("lastName", user.getLastName()).
                element("label", user.getLabel());
    }
}
