package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.Questionnaire;
import pl.dmcs.portal.model.QuestionnaireEntry;
import pl.dmcs.portal.service.QuestionnaireService;

@Controller
@RequestMapping("/courses/{courseId}/questionnaires")
public class QuestionnaireController extends AbstractController<Questionnaire> {

    @Autowired
    QuestionnaireService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse create(@PathVariable Long courseId, @RequestBody Map<String, Object> values) {
        Questionnaire q = service.create(courseId, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(service.read(courseId, q.getId())), true)).setHeader("Location", "svc/courses/" + courseId + "/questionnaires/" + q.getId());
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @PathVariable Long courseId,
            @RequestParam(defaultValue = "date") String sort,
            @RequestParam(defaultValue = "DESC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(courseId, sort, dir, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long courseId, @PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(courseId, id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long id, @RequestBody Map<String, Object> values) {
        Questionnaire q = service.update(id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(q), true));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{questionnaireId}/answer/{answerId}")
    public HttpResponse answer(@PathVariable Long courseId, @PathVariable Long questionnaireId, @PathVariable Long answerId) {
        service.answer(courseId, questionnaireId, answerId);
        return new JsonResponse(HttpServletResponse.SC_OK);
    }

    @Override
    public JSONObject getJsonRepresentation(Questionnaire t) {
        JSONObject result = new JSONObject().element("id", t.getId()).
                element("question", t.getQuestion()).
                element("date", dateTimeFormatter.print(t.getDate())).
                element("entries", new JSONArray()).
                element("course", new JSONObject().element("id", t.getCourse().getId()));

        if (t.getEntries() != null) {
            for (QuestionnaireEntry qe : t.getEntries()) {
                result.accumulate("entries", new JSONObject().element("id", qe.getId()).element("answer", qe.getAnswer()).element("total", qe.getTotal()));
            }
        }

        return result;
    }
}
