package pl.dmcs.portal.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.CourseAssignment;
import pl.dmcs.portal.model.TestResult;
import pl.dmcs.portal.service.TestResultService;

@Controller
@RequestMapping("/courses/{courseId}/tests/{testId}/results")
public class TestResultController extends AbstractController<TestResult> {

    @Autowired
    TestResultService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse create(@PathVariable Long courseId, @PathVariable Long testId, @RequestBody Map<String, Object> values) {
        TestResult t = service.create(testId, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(t), true)).setHeader("Location", "svc/courses/" + courseId + "/tests/" + testId + "/results/" + t.getId());
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @PathVariable Long courseId,
            @PathVariable Long testId,
            @RequestParam(value="group") Long groupId,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(courseId, groupId), testId));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long id, @RequestBody Map<String, Object> values) {
        TestResult g = service.update(id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(g), true));
    }

    @Override
    public JSONObject getJsonRepresentation(TestResult t) {
        JSONObject result = new JSONObject();
        result.element("id", t.getId()).element("result", t.getResult()).element("date", dateTimeFormatter.print(t.getDate()));
        result.element("assignment", t.getAssignment().getId());
        result.element("student", new JSONObject().element("id", t.getAssignment().getUser().getId()).
                element("label", t.getAssignment().getUser().getLabel()).
                element("indexNumber", t.getAssignment().getUser().getIndexNumber()));
        result.element("group", t.getAssignment().getGroup().getName());
        return result;
    }

    private JSONObject getJsonData(ArrayList<Object[]> list, Long testId) {
        JSONObject result = new JSONObject().element("data", new JSONArray());
        for (Object[] o : list) {
            CourseAssignment ca = (CourseAssignment) o[0];
            TestResult tr = (TestResult) o[1];
            JSONObject json = new JSONObject().element("assignment", ca.getId());
            json.element("student", new JSONObject().element("id", ca.getUser().getId()).
                    element("label", ca.getUser().getLabel()).
                    element("indexNumber", ca.getUser().getIndexNumber()));
            json.element("group", ca.getGroup().getName());
            if (tr != null && tr.getTest().getId().equals(testId)) {
                json = json.element("id", tr.getId()).
                        element("result", tr.getResult()).element("date", dateTimeFormatter.print(tr.getDate()));
            }
            result.accumulate("data", json);
        }
        return result;
    }
}
