package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.Message;
import pl.dmcs.portal.service.MessageService;

@Controller
@RequestMapping("/messages")
public class MessageController extends AbstractController<Message> {

    @Autowired
    MessageService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse create(@RequestBody Map<String, Object> values) {
        Message message = service.create(values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(message), true)).setHeader("Location", "svc/messages/" + message.getId());
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @RequestParam(defaultValue = "date") String sort,
            @RequestParam(defaultValue = "DESC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            @RequestParam(defaultValue = "inbox") String mode) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(sort, dir, start, limit, mode), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long id, @RequestBody Map<String, Object> values) {
        Message course = service.update(id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(course), true));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public HttpResponse delete(@PathVariable Long id) {
        service.delete(id);
        return new JsonResponse(HttpServletResponse.SC_OK);
    }

    @Override
    public JSONObject getJsonRepresentation(Message t) {
        JSONObject sender = new JSONObject().element("id", t.getSender().getId()).element("label", t.getSender().getLabel()).element("email", t.getSender().getEmail());
        JSONObject receiver = new JSONObject().element("id", t.getReceiver().getId()).element("label", t.getReceiver().getLabel()).element("email", t.getReceiver().getEmail());
        return new JSONObject().element("id", t.getId()).
                element("subject", t.getSubject()).
                element("text", t.getText()).
                element("read", t.isRead()).
                element("date", dateTimeFormatter.print(t.getDate())).
                element("sender", sender).
                element("receiver", receiver);
    }
}
