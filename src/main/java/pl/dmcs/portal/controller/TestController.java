package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.Test;
import pl.dmcs.portal.service.TestService;

@Controller
@RequestMapping("/courses/{courseId}/tests")
public class TestController extends AbstractController<Test> {

    @Autowired
    TestService service;

    @RequestMapping(method = RequestMethod.POST)
    public HttpResponse create(@PathVariable Long courseId, @RequestBody Map<String, Object> values) {
        Test t = service.create(courseId, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(t), true)).setHeader("Location", "svc/courses/" + courseId + "/tests/" + t.getId());
    }

    @RequestMapping(method = RequestMethod.GET)
    public HttpResponse list(
            @PathVariable Long courseId,
            @RequestParam(defaultValue = "id") String sort,
            @RequestParam(defaultValue = "DESC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(courseId, sort, dir, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpResponse read(@PathVariable Long id) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(service.read(id)), null));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public HttpResponse update(@PathVariable Long id, @RequestBody Map<String, Object> values) {
        Test g = service.update(id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(g), true));
    }

    @Override
    public JSONObject getJsonRepresentation(Test t) {
        return new JSONObject().element("id", t.getId()).
                element("name", t.getName()).
                element("description", t.getDescription()).
                element("type", t.getType()).
                element("date", t.getDate()).
                element("course", new JSONObject().
                element("id", t.getCourse().getId()).
                element("name", t.getCourse().getName()));
    }
}
