package pl.dmcs.portal.controller.support;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import static javax.servlet.http.HttpServletResponse.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import pl.dmcs.portal.service.UserService.DuplicatedEmailException;
import pl.dmcs.portal.service.support.BindingException;
import pl.dmcs.portal.service.support.UnknownParameterException;

public class ExceptionHandler implements HandlerExceptionResolver {

    static Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        View view = resolveView(request, response, handler, ex);
        if(request.getContentType() != null && request.getContentType().startsWith("multipart/form-data") && view instanceof JsonResponse) {
            ((JsonResponse)view).setContentType("text/html");
        }
        return new ModelAndView(view);
    }

    private View resolveView(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        JSONObject errorJson = new JSONObject().element("exception", ex.getClass().getSimpleName()).element("success", false);
        log.error(ex.getMessage(), ex);
        if (ex instanceof TypeMismatchException) {
            return new JsonResponse(SC_BAD_REQUEST, errorJson);
        } else if (ex instanceof BindingException) {
            errorJson.element("message", ex.getMessage());
            return new JsonResponse(SC_BAD_REQUEST, errorJson);
        } else if (ex instanceof UnknownParameterException) {
            errorJson.element("message", ex.getMessage());
            return new JsonResponse(SC_BAD_REQUEST, errorJson);
        } else if (ex instanceof IllegalArgumentException) {
            return new JsonResponse(SC_BAD_REQUEST, errorJson);
        } else if (ex instanceof DuplicatedEmailException) {
            return new JsonResponse(SC_CONFLICT, errorJson);
        } else if (ex instanceof EntityNotFoundException) {
            return new JsonResponse(SC_NOT_FOUND, errorJson);
        } else if (ex instanceof AccessDeniedException) {
            errorJson.element("message", ex.getMessage());
            return new JsonResponse(SC_UNAUTHORIZED, errorJson);
        } else if (ex instanceof AuthenticationCredentialsNotFoundException) {
            return new JsonResponse(SC_UNAUTHORIZED, errorJson);
        } else if (ex instanceof EntityExistsException) {
            return new JsonResponse(SC_CONFLICT, errorJson);
        } else if (ex instanceof HttpRequestMethodNotSupportedException) {
            return new JsonResponse(SC_BAD_REQUEST, errorJson);
        } else if (ex instanceof NoResultException) {
            return new JsonResponse(SC_NOT_FOUND, errorJson);
        } else {
            return new JsonResponse(SC_INTERNAL_SERVER_ERROR, errorJson);
        }
    }
}
