package pl.dmcs.portal.controller.support;

import java.util.Map;

import org.springframework.web.servlet.View;

public interface HttpResponse extends View {

    public String getHeader(String name);

    public Map<String, String> getHeaders();  

    public int getStatusCode();

}
