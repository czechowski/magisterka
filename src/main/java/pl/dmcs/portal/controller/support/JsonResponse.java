package pl.dmcs.portal.controller.support;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

public class JsonResponse extends AbstractHttpResponse {

   String json;
   private String contentType = "application/json";

    public JsonResponse(int statusCode, JSONObject json) {
        super(statusCode);
        this.json = json.toString();
    }

    public JsonResponse(int statusCode, String json) {
        super(statusCode);
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    /** 
     * Empty response
     */
    public JsonResponse(int statusCode) {
        super(statusCode);
        this.json = statusCode < 400 ? "{\"success\":true}" : "{\"success\":false}";
    }

    @Override
    public String getContentType() {
        return contentType;
    }


    public JsonResponse setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    @Override
    public void renderBody(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType(getContentType());
        response.getWriter().write(json);
    }

}
