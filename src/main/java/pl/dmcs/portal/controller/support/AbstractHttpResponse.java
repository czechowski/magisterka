package pl.dmcs.portal.controller.support;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractHttpResponse implements HttpResponse {

    private int statusCode;
    private Map<String, String> headers;

    public AbstractHttpResponse(int statusCode) {
        this.statusCode = statusCode;
        this.headers = new HashMap<String, String>();
    }

    public AbstractHttpResponse(int statusCode, Map<String, String> headers) {
        this.statusCode = statusCode;
        this.headers = headers;
    }

    @Override
    public String getContentType() { //NOPMD default impl
        return null;
    }


    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setStatus(statusCode);
        if (headers != null) {
            for (Map.Entry<String, String> header : headers.entrySet()) {
                response.setHeader(header.getKey(), header.getValue());
            }
        }
        renderBody(model, request, response);
    }

    protected abstract void renderBody(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception;

    public AbstractHttpResponse setHeader(String name, String value) {
        headers.put(name, value);
        return this;
    }

    @Override
    public String getHeader(String name) {
        return headers.get(name);
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
