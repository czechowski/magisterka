package pl.dmcs.portal.controller.support;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import pl.dmcs.portal.service.support.PartialList;

public abstract class AbstractController<T> {

    public DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();

    public JSONObject getJsonData(Iterable<T> records, Boolean success) {
        JSONObject data = new JSONObject().element("data", new JSONArray());
        if (success != null) {
            data.element("success", success.booleanValue());
        }
        for (T t : records) {
            data.accumulate("data", getJsonRepresentation(t));
        }
        if (records instanceof PartialList) {
            data.element("total", ((PartialList) records).getTotalSize());
        }
        return data;
    }

    public abstract JSONObject getJsonRepresentation(T t);
}
