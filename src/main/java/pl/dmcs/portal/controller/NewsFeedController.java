package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.NewsContent;
import pl.dmcs.portal.service.NewsFeedService;

@Controller
public class NewsFeedController extends AbstractController<NewsContent> {

    @Autowired
    NewsFeedService service;

    @RequestMapping(method = RequestMethod.GET, value = "/newsfeed")
    public HttpResponse list(@RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(start, limit), null));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/courses/{courseId}/newsfeed")
    public HttpResponse list(@PathVariable Long courseId,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "30") Integer limit) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(service.list(courseId, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/courses/{courseId}/newsfeed")
    public HttpResponse create(@PathVariable Long courseId, @RequestBody Map<String, Object> values) {
        NewsContent nc = service.create(courseId, values);
        return new JsonResponse(HttpServletResponse.SC_CREATED, getJsonData(Arrays.asList(nc), true)).setHeader("Location", "svc/courses/" + courseId + "/newsfeed/" + nc.getId());
    }

    @Override
    public JSONObject getJsonRepresentation(NewsContent t) {
        return new JSONObject().element("id", t.getId()).element("content", t.getContent()).
                element("date", dateTimeFormatter.print(t.getDate())).
                element("course", new JSONObject().element("id", t.getCourse().getId()).element("name", t.getCourse().getName()));
    }
}
