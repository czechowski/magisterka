package pl.dmcs.portal.controller;

import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.portal.controller.support.AbstractController;
import pl.dmcs.portal.controller.support.HttpResponse;
import pl.dmcs.portal.controller.support.JsonResponse;
import pl.dmcs.portal.model.CourseAssignment;
import pl.dmcs.portal.service.CourseAssignmentService;

@Controller
public class CourseAssignmentController extends AbstractController<CourseAssignment> {

    @Autowired
    CourseAssignmentService courseAssignmentService;
    @Autowired
    UserController userController;

    @RequestMapping(method = RequestMethod.GET, value = "/courses/{courseId}/users")
    public HttpResponse list(
            @PathVariable Long courseId,
            @RequestParam(value="group") Long groupId,
            @RequestParam(defaultValue = "user.lastName") String sort,
            @RequestParam(defaultValue = "ASC") String dir,
            @RequestParam(defaultValue = "0") Integer start,
            @RequestParam(defaultValue = "50") Integer limit,
            HttpServletRequest request) {
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(courseAssignmentService.list(courseId, groupId, sort, dir, start, limit), null));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/courses/{courseId}/groups/{groupId}/users")
    public HttpResponse createAssignment(@PathVariable Long courseId, @PathVariable Long groupId) {
        courseAssignmentService.createAssignment(courseId, groupId);
        return new JsonResponse(HttpServletResponse.SC_CREATED);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/courses/{courseId}/groups/{groupId}/users/{userId}")
    public HttpResponse createAssignment(@PathVariable Long courseId, @PathVariable Long groupId, @PathVariable Long userId) {
        courseAssignmentService.createAssignment(courseId, groupId, userId);
        return new JsonResponse(HttpServletResponse.SC_CREATED);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/courses/{courseId}/groups/{groupId}/users/{userId}/confirm")
    public HttpResponse confirmAssignment(@PathVariable Long courseId, @PathVariable Long groupId, @PathVariable Long userId) {
        courseAssignmentService.confirmAssignment(courseId, groupId, userId);
        return new JsonResponse(HttpServletResponse.SC_OK);
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/courses/{courseId}/users/{id}")
    public HttpResponse update(@PathVariable Long courseId, @PathVariable Long id, @RequestBody Map<String, Object> values) {
        CourseAssignment ca = courseAssignmentService.update(courseId, id, values);
        return new JsonResponse(HttpServletResponse.SC_OK, getJsonData(Arrays.asList(ca), true));
    }

    @Override
    public JSONObject getJsonRepresentation(CourseAssignment t) {
        return new JSONObject().element("id", t.getId()).element("confirmed", t.isConfirmed()).
                element("course", new JSONObject().element("name", t.getCourse().getName()).element("id", t.getCourse().getId())).
                element("group", new JSONObject().element("id", t.getGroup().getId()).element("name", t.getGroup().getName()).element("year", t.getGroup().getYear())).
                element("user", userController.getJsonRepresentation(t.getUser()));
    }
}
