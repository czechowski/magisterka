Ext.ns('Portal.windows');

Portal.windows.RegistrationWindow = Ext.extend(Ext.Window, {
    constructor: function(config) {
        config = config || {};

        var setLabel = function(formPanel) {
            var title = (formPanel.getComponent('type').getValue().inputValue == 'LECTURER' ? formPanel.getComponent('title').getValue(): '')
            var label = [title ? title + ' ' : '',
            formPanel.getComponent('firstName').getValue() + ' ',
            formPanel.getComponent('lastName').getValue()].join('');
            formPanel.getComponent('label').setValue(label);
        }

        this.formPanel = new Ext.form.FormPanel({
            defaults: {
                xtype: 'textfield',
                anchor: '100%'
            },
            items: [{
                xtype: 'radiogroup',
                fieldLabel: 'Typ użytkownika',
                itemId: 'type',
                columns: 2,
                defaults: {
                    listeners: {
                        check: function(radio, checked) {
                            if(radio.inputValue === 'STUDENT') {
                                this.formPanel.getComponent('indexNumber').setVisible(checked);
                            } else {
                                this.formPanel.getComponent('title').setVisible(checked);
                            }
                        },
                        scope: this
                    }
                },
                items: [{
                    boxLabel: 'Student',
                    inputValue: 'STUDENT',
                    name: 'type',
                    checked: true
                },{
                    boxLabel: 'Wykładowca',
                    inputValue: 'LECTURER',
                    name: 'type'
                }]
            },{
                itemId: 'email',
                fieldLabel: 'E-mail',
                vtype: 'email',
                maxLength: 100,
                allowBlank: false
            },{
                inputType : 'password',
                fieldLabel: 'Hasło',
                itemId: 'password',
                minLength: 6,
                maxLength: 100,
                allowBlank: false
            },{
                inputType : 'password',
                fieldLabel: 'Powtórz hasło',
                itemId: 'passwordConfirm',
                allowBlank: false,
                minLength: 6,
                maxLength: 100,
                validator: (function(value) {
                    if(value !== this.formPanel.getComponent('password').getValue()) {
                        return 'Hasła nie są identyczne';
                    }
                    return true;
                }).createDelegate(this)
            },{
                itemId: 'firstName',
                fieldLabel: 'Imię',
                allowBlank: false,
                maxLength: 50,
                enableKeyEvents: true,
                listeners: {
                    scope: this,
                    keyup: function() {
                        setLabel(this.formPanel);
                    }
                }
            },{
                fieldLabel: 'Nazwisko',
                itemId: 'lastName',
                maxLength: 50,
                allowBlank: false,
                enableKeyEvents: true,
                listeners: {
                    scope: this,
                    keyup: function() {
                        setLabel(this.formPanel);
                    }
                }
            },{
                fieldLabel: 'Etykieta',
                allowBlank: false,
                maxLength: 125,
                itemId: 'label'
            },{
                fieldLabel: 'Numer indeksu',
                itemId: 'indexNumber',
                maxLength: 10
            },{
                fieldLabel: 'Tytuł naukowy',
                itemId: 'title',
                hidden: true,
                maxLength: 20,
                enableKeyEvents: true,
                listeners: {
                    scope: this,
                    keyup: function() {
                        setLabel(this.formPanel);
                    }
                }
            }],
            buttons: [{
                text: 'Zarejestruj',
                formBind: true,
                handler: function() {
                    var params = {
                        firstName: this.formPanel.getComponent('firstName').getValue(),
                        lastName: this.formPanel.getComponent('lastName').getValue(),
                        label: this.formPanel.getComponent('label').getValue(),
                        email: this.formPanel.getComponent('email').getValue(),
                        password: this.formPanel.getComponent('password').getValue(),
                        type: this.formPanel.getComponent('type').getValue().inputValue
                    };
                    if(params.type === 'STUDENT') {
                        params.indexNumber = this.formPanel.getComponent('indexNumber').getValue();
                    } else {
                        params.title = this.formPanel.getComponent('title').getValue();
                    }
                    Portal.common.Auth.register(params);
                },
                scope: this
            }]
        });

        Ext.apply(config, {
            title: 'Rejestracja',
            width: 400,
            id: 'portal.windows.registrationwindow',
            modal: true,
            items: [this.formPanel]
        });

        Portal.windows.RegistrationWindow.superclass.constructor.call(this, config);
    }
});
