Ext.ns('Portal.windows');

Portal.windows.TestCreateWindow = Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {

        config = Ext.apply({
            title: 'Nowe zaliczenie',
            width: 400,
            iconCls: 'test-icon',
            fields: [{
                xtype: 'textfield',
                name: 'name',
                maxLength: 50,
                allowBlank: false,
                fieldLabel: 'Nazwa'
            },{
                xtype: 'combo',
                name: 'type',
                fieldLabel: 'Typ zaliczenia',
                triggerAction: 'all',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    data: [['Egzamin', 'Egzamin'], ['Zaliczenie', 'Zaliczenie'], ['Projekt', 'Projekt']],
                    fields: ['value', 'label']
                }),
                displayField: 'label',
                valueField: 'value',
                hiddenName: 'type'
            },{
                xtype: 'textfield',
                name: 'date',
                fieldLabel: 'Termin(y)',
                maxLength: 100
            },{
                xtype: 'textarea',
                name: 'description',
                maxLength: 100,
                height: 100,
                fieldLabel: 'Opis'
            }],
            defaultButton: 'name'
        }, config);

        Portal.windows.TestCreateWindow.superclass.constructor.call(this, config);
    }
});