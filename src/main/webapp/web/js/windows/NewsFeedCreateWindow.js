Ext.ns('Portal.windows');

Portal.windows.NewsFeedCreateWindow = Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {
        
        config = Ext.apply({
            title: 'Dodaj ogłoszenie',
            width: 500,
            fields: [{
                xtype: 'textarea',
                name: 'content',
                maxLength: 1000,
                height: 150,
                allowBlank: false,
                fieldLabel: 'Treść ogłoszenia'
            }],
            iconCls: 'feed-icon',
            defaultButton: 'content',
            createHandler: function() {
                var values = this.formPanel.getForm().getValues();
                this.store.insert(0, new this.store.recordType(values));
                this.close();
            }
        }, config);

        Portal.windows.NewsFeedCreateWindow.superclass.constructor.call(this, config);
    }
});