Ext.ns('Portal.windows');

Portal.windows.FileUploadWindow = Ext.extend(Ext.Window, {
    url: '',
    enableDescription: false,
    constructor: function(config) {
        config = config || {};

        var onUploadSuccess = (config.onUploadSuccess || Ext.emptyFn).createDelegate(config.scope || this);
        var onUploadFailure = (config.onUploadSuccess || Ext.emptyFn).createDelegate(config.scope || this);

        var items = [{
            xtype: 'fileuploadfield',
            fieldLabel: 'Wybierz plik',
            name: 'file',
            buttonText: '',
            buttonCfg: {
                iconCls: 'browse-icon'
            }
        }];

        if(config.enableDescription === true) {
            items.push({
                xtype: 'textfield',
                name: 'description',
                fieldLabel: 'Opis',
                maxLength: 250,
                allowBlank: true
            });
        }

        this.formPanel = new Ext.FormPanel({
            fileUpload: true,
            frame: true,
            autoHeight: true,
            bodyStyle: 'padding: 10px 10px 0 10px;',
            labelWidth: 50,
            waitTitle: 'Wysyłanie pliku',
            defaults: {
                anchor: '100%',
                allowBlank: false
            },
            items: items,
            buttons: [{
                text: 'Dodaj',
                scope: this,
                formBind: true,
                handler: function(){
                    this.hide();
                    //set authentication cookie
                    Portal.common.Auth.setAuthorizationCookie();
                    this.formPanel.getForm().submit({
                        url: config.url || this.url,
                        waitMsg: 'Proszę czekać...',
                        scope: this,
                        success: function(fp, o){
                            this.close();
                            onUploadSuccess();
                        },
                        failure: function(form, action) {
                            onUploadFailure();
                            var msg = 'Nie udało się wysłać pliku.';
                            if(Ext.isObject(action.result) && action.result.exception === 'IllegalArgumentException') {
                                msg += ' Rozmiar pliku został przekroczony. Maksymalny rozmiar to 16MB.'
                            }
                            Ext.MessageBox.confirm('Wysyłanie pliku', msg + ' Czy chcesz spróbować jeszcze raz?', function(response) {
                                this[response === 'yes' ? 'show' : 'close']();
                            }, this).setIcon(Ext.MessageBox.ERROR);
                        }
                    });
                }
            }]
        });

        config = Ext.apply({
            title: 'Dodawanie pliku',
            items: [this.formPanel],
            width: 400,
            modal: true
        }, config);
        
        Portal.windows.FileUploadWindow.superclass.constructor.call(this, config);
    }
});
