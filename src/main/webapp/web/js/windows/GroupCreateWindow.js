Ext.ns('Portal.windows');

Portal.windows.GroupCreateWindow = Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {

        config = Ext.apply({
            title: 'Dodaj nową grupę',
            width: 300,
            iconCls: 'group-icon',
            fields: [{
                xtype: 'textfield',
                name: 'name',
                maxLength: 30,
                allowBlank: false,
                fieldLabel: 'Nazwa grupy'
            },{
                xtype: 'textfield',
                name: 'year',
                maxLength: 20,
                fieldLabel: 'Rok akademicki'
            },{
                xtype: 'textfield',
                name: 'date',
                maxLength: 20,
                fieldLabel: 'Termin zajęć'
            },{
                xtype: 'textfield',
                name: 'room',
                maxLength: 20,
                fieldLabel: 'Sala'
            }],
            defaultButton: 'name'
        }, config);

        Portal.windows.GroupCreateWindow.superclass.constructor.call(this, config);
    }
});