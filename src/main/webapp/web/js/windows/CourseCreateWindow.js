Ext.ns('Portal.windows');

Portal.windows.CourseCreateWindow = Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {
        config = config || {};

        var fields = [{
            xtype: 'textfield',
            name: 'name',
            maxLength: 100,
            allowBlank: false,
            fieldLabel: 'Nazwa przedmiotu'
        },{
            xtype: 'textfield',
            name: 'unitName',
            maxLength: 100,
            fieldLabel: 'Jednostka organizacyjna'
        },{
            fieldLabel: 'Rodzaj zajęć',
            name: 'type',
            xtype: 'combo',
            triggerAction: 'all',
            mode: 'local',
            store: new Ext.data.ArrayStore({
                data: [['LECTURE', 'Wykład'], ['EXERCISES', 'Ćwiczenia'], ['LABORATORY', 'Laboratorium']],
                fields: ['value', 'label']
            }),
            displayField: 'label',
            valueField: 'value',
            hiddenName: 'type'
        },{
            xtype: 'htmleditor',
            name: 'description',
            height: 250,
            fieldLabel: 'Opis przedmiotu',
            validator: function(value) {
                if(value.length > 10000) {
                    return 'Długość opisu jest zbyt duża (obecnie: ' + value.length + ' znaków, max: 10000)';
                }
                return true;
            }
        }];

        Ext.apply(config, {
            title: 'Dodaj nowy przedmiot',
            width: 600,
            iconCls: 'course-icon',
            fields: fields,
            defaultButton: 'name'
        });

        Portal.windows.CourseCreateWindow.superclass.constructor.call(this, config);
    }
});