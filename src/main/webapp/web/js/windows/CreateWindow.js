Ext.ns('Portal.windows');

Portal.windows.CreateWindow = Ext.extend(Ext.Window, {
    constructor: function(config) {

        this.formPanel = new Ext.form.FormPanel(Ext.apply({
            autoHeight: true,
            defaults: {
                anchor: '100%'
            },
            cls: 'displayfield-new-look',
            items: config.fields,
            buttonAlign: 'right',
            buttons: [{
                text: 'OK',
                handler: config.createHandler || this.createHandler,
                scope: this,
                ref: 'createButton',
                formBind: true
            }]
        }, config.formPanelConfig));

        if(Ext.isString(config.defaultButton)) {
            var defaultField = this.formPanel.form.findField(config.defaultButton);
            if(defaultField) {
                config.defaultButton = defaultField;
            } else {
                delete config.defaultButton;
            }
        }

        config = Ext.apply({
            items: [this.formPanel],
            autoHeight: true,
            modal: true
        }, config);

        Portal.windows.CreateWindow.superclass.constructor.call(this, config);

        this.show();
        this.center();
    },
    createHandler: function() {
        if(this.store && this.store.recordType) {
            var values = this.formPanel.getForm().getValues();
            this.store.add(new this.store.recordType(this.processValues(values)));
        }
        this.close();
    },
    processValues: function(values) {
        return values;
    }
});