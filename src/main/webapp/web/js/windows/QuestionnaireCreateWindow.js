Ext.ns('Portal.windows');

Portal.windows.QuestionnaireCreateWindow = Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {

        var initData = [];
        for(var iter = 0; iter < 6; iter++) {
            initData.push({
                answer: ''
            });
        }

        var entriesGrid = new Ext.grid.EditorGridPanel({
            header: false,
            border: true,
            bodyStyle: 'border: 1px solid #99BBE8;',
            fieldLabel: 'Oczekiwane odpowiedzi',
            clicksToEdit: 1,
            height: 200,
            viewConfig: {
                autoFill: true,
                markDirty: false
            },
            store: new Ext.data.JsonStore({
                fields: ['answer'],
                root: 'data',
                restful: true,
                data: {
                    data: initData
                }
            }),
            columns: [{
                header: 'Odpowiedź',
                dataIndex: 'answer',
                editor: {
                    xtype: 'textfield',
                    maxLength: 250
                }
            }],
            bbar: [{
                text: 'Dodaj następną',
                handler: function() {
                    entriesGrid.store.add(new entriesGrid.store.recordType({
                        answer: ''
                    }));
                }
            }]
        });

        config = Ext.apply({
            title: 'Nowa ankieta',
            width: 600,
            iconCls: 'questionnaire-icon',
            fields: [{
                xtype: 'textarea',
                name: 'question',
                maxLength: 500,
                height: 80,
                fieldLabel: 'Pytanie'
            },entriesGrid],
            defaultButton: 'question',
            createHandler: function() {
                if(this.store && this.store.recordType) {
                    var values = this.formPanel.getForm().getValues();
                    var records = entriesGrid.store.getRange();
                    values.entries = [];
                    for(var iter = 0; iter < records.length; iter++) {
                        if(records[iter].data.answer !== '') {
                            values.entries.push(records[iter].data.answer);
                        }
                    }
                    this.store.add(new this.store.recordType(this.processValues(values)));
                }
                this.close();
            }
        }, config);

        Portal.windows.QuestionnaireCreateWindow.superclass.constructor.call(this, config);
    }
});