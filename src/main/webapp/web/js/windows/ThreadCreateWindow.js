Ext.ns('Portal.windows');

Portal.windows.ThreadCreateWindow = Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {

        config = Ext.apply({
            title: config.parentRecord ? 'Odpowiedz' : 'Nowy wątek',
            width: 600,
            iconCls: 'forum-icon',
            fields: [{
                xtype: config.parentRecord ? 'displayfield' : 'textfield',
                name: 'subject',
                maxLength: 100,
                allowBlank: false,
                fieldLabel: 'Temat',
                value: config.parentRecord ? config.parentRecord.data.subject : ''
            },{
                xtype: 'htmleditor',
                name: 'text',
                height: 250,
                maxLength: 5000,
                fieldLabel: 'Treść wiadomości'
            }],
            defaultButton: 'name'
        }, config);

        Portal.windows.ThreadCreateWindow.superclass.constructor.call(this, config);
    },
    processValues: function(values) {
        if(this.parentRecord) {
            return Ext.apply(values, {
                subject: this.parentRecord.data.subject
            });
        }
        return values;
    }
});