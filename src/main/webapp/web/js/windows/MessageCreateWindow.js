Ext.ns('Portal.windows');

Portal.windows.MessageCreateWindow =  Ext.extend(Portal.windows.CreateWindow, {
    constructor: function(config) {
        config = config || {};

        var values = config.values || {};

        var fields = [{
            xtype: 'contactscombobox',
            name: 'receiver',
            hiddenName: 'receiver',
            maxLength: 50,
            allowBlank: false,
            fieldLabel: 'Adres e-mail odbiorcy',
            value: values.receiver || '',
            hiddenValue: values.receiver || ''
        },{
            xtype: 'textfield',
            name: 'subject',
            maxLength: 100,
            fieldLabel: 'Temat',
            value: values.subject || ''
        },{
            xtype: 'htmleditor',
            name: 'text',
            height: 250,
            maxLength: 3000,
            fieldLabel: 'Treść wiadomości',
            value: values.text || ''
        }];

        Ext.apply(config, {
            title: 'Utwórz wiadomość',
            width: 600,
            fields: fields,
            defaultButton: 'receiver',
            iconCls: 'messages-icon'
        });

        Portal.windows.MessageCreateWindow.superclass.constructor.call(this, config);
    },
    createHandler: function() {
        var values = this.formPanel.getForm().getValues();
        this.body.mask('Wysyłanie wiadomości...', 'x-mask-loading');
        Ext.Ajax.request({
            method: 'POST',
            jsonData: values,
            url: 'svc/messages',
            scope: this,
            success: function() {
                this.body.unmask();
                this.close();
            },
            failure: function(response) {
                this.body.unmask();
                if(response.status === 404) {
                    Ext.MessageBox.alert("Błąd podczas wysyłania wiadomości", "Podany adres e-mail nie istnieje w systemie. Wiadomość nie została wysłana.").setIcon(Ext.MessageBox.ERROR);
                } else {
                    Ext.MessageBox.alert("Błąd podczas wysyłania wiadomości", "Wystąpił nieokreślony błąd. Wiadomość nie została wysłana.").setIcon(Ext.MessageBox.ERROR);
                }
            }
        });
    }
});