Ext.ns('Portal.windows');

Portal.windows.LoginWindow = Ext.extend(Ext.Window, {
    constructor: function(config) {
        config = config || {};

        this.formPanel = new Ext.form.FormPanel({
            defaults: {
                anchor: '100%'
            },
            items: [{
                xtype: 'textfield',
                itemId: 'email',
                fieldLabel: 'E-mail',
                vtype: 'email',
                allowBlank: false
            },{
                xtype: 'textfield',
                inputType : 'password',
                fieldLabel: 'Hasło',
                itemId: 'password',
                allowBlank: false
            },{
                xtype: 'checkbox',
                boxLabel: 'Zapamiętaj',
                itemId: 'remember',
                hideLabel: true
            }],
            buttons: [{
                text: 'Nowe konto',
                handler: function() {
                    (Ext.WindowMgr.get('portal.windows.registrationwindow') || new Portal.windows.RegistrationWindow()).show();
                }
            },{
                text: 'OK',
                formBind: true,
                handler: function() {
                    this.el.mask('Proszę czekać...','x-mask-loading');
                    Portal.common.Auth.on('loginsuccessful', function() {
                        this.close();
                    }, this, {
                        single: true
                    });
                    Portal.common.Auth.on('loginfailure', function() {
                        this.el.unmask();
                    }, this, {
                        single: true
                    });

                    Portal.common.Auth.login({
                        email: this.formPanel.getComponent('email').getValue(),
                        password: this.formPanel.getComponent('password').getValue()
                    }, this.formPanel.getComponent('remember').getValue());
                },
                scope: this
            }]
        });

        Ext.apply(config, {
            items: [this.formPanel],
            width: 300,
            closable: false,
            modal: true,
            title: 'Logowanie',
            id: 'portal.windows.loginwindow'
        });

        Portal.windows.LoginWindow.superclass.constructor.call(this, config);
    }
});


