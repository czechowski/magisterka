Ext.ns('Portal.grids');

Portal.grids.CourseGroups = Ext.extend(Ext.grid.EditorGridPanel, {
    constructor: function(config) {
        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/courses/{course}/groups',
            fields: ['name', 'date', 'room', 'year'],
            restful: true,
            writer: new Portal.common.XJsonWriter()
        });

        config = Ext.apply(config || {}, {
            store: store,
            title: 'Grupy',
            stripeRows: true,
            viewConfig: {
                autoFill: true
            },
            selModel: Portal.common.Auth.isLecturer() ? new Ext.grid.CellSelectionModel() : new Ext.grid.RowSelectionModel(),
            columns: [{
                header: 'Nazwa grupy',
                dataIndex: 'name',
                width: 200,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 30
                } : undefined
            },{
                header: 'Rok akademicki',
                dataIndex: 'year',
                width: 200,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 10
                } : undefined
            },{
                header: 'Termin zajęć',
                dataIndex: 'date',
                width: 200,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 20
                }: undefined
            },{
                header: 'Sala',
                dataIndex: 'room',
                width: 200,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 20
                } : undefined
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Dodaj grupę',
                    tooltip: 'Kliknij, aby dodać grupę lektorską',
                    scope: this,
                    iconCls: 'group-add-icon',
                    hidden: !Portal.common.Auth.isLecturer(),
                    handler: function() {
                        new Portal.windows.GroupCreateWindow({
                            store: store
                        });
                    }
                },new Ext.Toolbar.Separator({
                    hidden: Portal.common.Auth.isLecturer()
                }),{
                    hidden: Portal.common.Auth.isLecturer(),
                    text: 'Zapisz się na zajęcia',
                    tooltip: 'Kliknij, aby zapisać się na zajęcia do wybranej grupy',
                    scope: this,
                    handler: function() {
                        var record = this.getSelectionModel().getSelected();
                        if(record) {
                            Ext.Ajax.request({
                                url: this.store.proxy.api.create.url + '/' + record.id + '/users',
                                method: 'POST',
                                success: function() {
                                    Ext.MessageBox.alert("Potwierdzenie", "Zostałeś zapisany do grupy " + record.data.name).setIcon(Ext.MessageBox.INFO);
                                },
                                failure: function() {
                                    Ext.MessageBox.alert("Potwierdzenie", "Jesteś już zapisany do grupy " + record.data.name).setIcon(Ext.MessageBox.WARNING);
                                }
                            });
                        }
                    },
                    iconCls: 'user-add-icon',
                    disabled: true,
                    itemId: 'userAddButton'
                }]
            })
        });

        Portal.grids.CourseGroups.superclass.constructor.call(this, config);

        if(!Portal.common.Auth.isLecturer()) {
            this.getSelectionModel().on('rowselect', function() {
                this.topToolbar.getComponent('userAddButton').enable();
            }, this);
            this.getSelectionModel().on('rowdeselect', function() {
                this.topToolbar.getComponent('userAddButton').disable();
            }, this);
        }

    },
    setCourseId: function(courseId) {
        var url = 'svc/courses/' + courseId + '/groups';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.store.reload();
    }
});