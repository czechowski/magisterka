Ext.ns('Portal.grids');

Portal.grids.Questionnaires = Ext.extend(Ext.Panel, {
    constructor: function(config) {
        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/courses/{courseId}/questionnaires',
            fields: ['id', 'question', 'entries', {
                name: 'date',
                type: 'date',
                format: 'c'
            },{
                name: 'courseId',
                mapping: 'course.id'
            }],
            restful: true,
            writer: new Portal.common.XJsonWriter()
        });

        this.grid = new Ext.grid.GridPanel({
            store: store,
            region: 'center',
            header: false,
            stripeRows: true,
            viewConfig: {
                autoFill: true
            },
            columns: [new Ext.grid.RowNumberer(), {
                header: 'Pytanie',
                dataIndex: 'question',
                width: 600
            },{
                header: 'Data',
                dataIndex: 'date',
                width: 200,
                renderer: function(value) {
                    return Ext.isDate(value) ? value.format('d.m.Y') : new Date().format('d.m.Y');
                }
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Nowa ankieta',
                    tooltip: 'Kliknij, aby utworzyć nową ankietę',
                    scope: this,
                    iconCls: 'questionnaire-add-icon',
                    hidden: !Portal.common.Auth.isLecturer(),
                    handler: function() {
                        new Portal.windows.QuestionnaireCreateWindow({
                            store: store
                        });
                    }
                }]
            })
        });

        this.grid.getSelectionModel().on('rowselect', function(sm, index, record) {
            this.chartStore.loadData(record.data);
            this.loadForm(record.data);
        }, this);

        this.formPanel = new Ext.form.FormPanel({
            labelAlign: 'top',
            buttonAlign: 'left',
            border: false,
            frame: false,
            monitorValid: true,
            labelWidth: 150,
            padding: 15,
            cls: 'displayfield-new-look',
            items: [],
            flex: 1,
            autoScroll: true,
            listeners: {
                clientvalidation: function(form, valid) {
                    if(form.getComponent('sendAnswer')) {
                        form.getComponent('sendAnswer').setDisabled(!valid);
                    }
                },
                scope: this
            }
        });

        this.chartStore = new Ext.data.JsonStore({
            fields: ['answer', 'total'],
            root: 'entries'
        });

        this.chart = new Ext.chart.PieChart({
            store: this.chartStore,
            dataField: 'total',
            flex: 1,
            disableCaching: true,
            categoryField: 'answer',
            extraStyle: {
                padding: 20,
                legend: {
                    display: 'left',
                    font: {
                        family: 'Tahoma',
                        size: 13
                    }
                }
            }
        });
        
        this.bottomPanel = new Ext.Panel({
            layout: 'hbox',
            split: true,
            layoutConfig: {
                align: 'stretch'
            },
            region: 'south',
            height: 300,
            defaults: {
                bodyStyle: 'background-color: #FFFFFF;'
            },
            items: [this.formPanel, this.chart]
        });

        config = Ext.apply(config || {}, {
            title: 'Ankiety',
            layout: 'border',
            items: [this.grid, this.bottomPanel],
            defaults: {
                border: false
            }
        });

        Portal.grids.Questionnaires.superclass.constructor.call(this, config);

    },
    setCourseId: function(courseId) {
        var url = 'svc/courses/' + courseId + '/questionnaires';
        this.grid.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.grid.topToolbar.moveFirst();
    },
    loadForm: function(data) {
        var items = [];
        for(var iter = 0; iter < data.entries.length; iter++) {
            items.push({
                boxLabel: data.entries[iter].answer,
                name: data.id,
                inputValue: data.entries[iter].id
            });
        }
        this.formPanel.removeAll();
        this.formPanel.add({
            xtype: 'radiogroup',
            fieldLabel: data.question,
            columns: 1,
            items: items,
            allowBlank: false
        },{
            xtype: 'button',
            itemId: 'sendAnswer',
            disabled: true,
            text: 'Wyślij odpowiedź',
            handler: function() {
                if(this.formPanel.items.get(0).getValue()) {
                    this.answer(this.formPanel.items.get(0).getValue().inputValue);
                }
            },
            scope: this
        });
        this.bottomPanel.doLayout();
    },
    answer: function(answerId) {
        if(this.grid.getSelectionModel().hasSelection()) {
            var record = this.grid.getSelectionModel().getSelected();
            Ext.Ajax.request({
                url: 'svc/courses/' + record.data.courseId + '/questionnaires/' + record.id + '/answer/' + answerId,
                method: 'PUT',
                scope: this,
                success: function() {
                    var data = record.data;
                    for(var iter = 0; iter < data.entries.length; iter++) {
                        if(data.entries[iter].id === answerId) {
                            data.entries[iter].total++;
                            this.chartStore.loadData(data);
                        }
                    }
                }
            });
        }
    }
});