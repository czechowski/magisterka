Ext.ns('Portal.grids');

Portal.grids.Messages = Ext.extend(Ext.grid.GridPanel, {
    constructor: function(config) {

        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/messages',
            fields: ['id', 'subject', 'text', 'sender', 'receiver', 'read', {
                name: 'date',
                type: 'date',
                format: 'c'
            }],
            restful: true,
            idProperty: 'id',
            writer: new Portal.common.XJsonWriter(),
            autoLoad: false,
            remoteSort: true
        });

        config = Ext.apply({
            id: 'messages',
            title: 'Wiadomości',
            store: store,
            loadMask: true,
            stripeRows: true,
            viewConfig: {
                autoFill: true,
                getRowClass: function(record, rowIndex, rp, ds){ // rp = rowParams
                if(record.get('read') !== true){
                    return 'messages-row-not-read';
                }
                return '';
            }
            },
            iconCls: 'messages-icon',
            columns: [new Ext.grid.RowNumberer(),{
                header: 'Nadawca',
                width: 50,
                dataIndex: 'sender',
                sortable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    if(Ext.isObject(record.get('sender'))) {
                        return Portal.common.Util.prepareObjectLink('user', record.get('sender').id, record.get('sender').label);
                    } else {
                        return value;
                    }                    
                }
            },{
                header: 'Odbiorca',
                width: 50,
                dataIndex: 'receiver',
                sortable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    if(Ext.isObject(record.get('receiver'))) {
                        return Portal.common.Util.prepareObjectLink('user', record.get('receiver').id, record.get('receiver').label);
                    } else {
                        return value;
                    }
                }
            },{
                header: 'Temat',
                dataIndex: 'subject',
                width: 200,
                sortable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return Portal.common.Util.prepareObjectLink('message', record.id, value || '(brak tematu)');
                }
            },{
                header: 'Data',
                dataIndex: 'date',
                width: 50,
                sortable: true,
                renderer: function(value) {
                    return Ext.isDate(value) ? value.format('d.m.Y H:i') : new Date().format('d.m.Y H:i');
                }
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    itemId: 'separator'
                }),{
                    text: 'Utwórz',
                    tooltip: 'Utwórz wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-add',
                    handler: function() {
                        new Portal.windows.MessageCreateWindow({
                            store: store
                        }).show();
                    }
                },'-',{
                    text: 'Odpowiedz',
                    tooltip: 'Odpowiedz na zaznaczoną wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-reply',
                    handler: function() {
                        var record = this.getSelectionModel().getSelected();
                        if(record) {
                            var values = {
                                receiver: record.data.sender.email || '',
                                subject: 'Re: ' + record.data.subject
                            }
                            new Portal.windows.MessageCreateWindow({
                                store: store,
                                values: values
                            }).show();
                        }
                    }
                },'-',{
                    text: 'Prześlij',
                    tooltip: 'Prześlij dalej zaznaczoną wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-forward',
                    handler: function() {
                        var record = this.getSelectionModel().getSelected();
                        if(record) {
                            var values = {
                                subject: 'Fwd: ' + record.data.subject,
                                text: record.data.text
                            }
                            new Portal.windows.MessageCreateWindow({
                                store: store,
                                values: values
                            }).show();
                        }
                    }
                },'-',{
                    text: 'Usuń',
                    tooltip: 'Usuń zaznaczone wiadomości',
                    scope: this,
                    iconCls: 'messages-icon-delete',
                    handler: function() {
                        var record = this.getSelectionModel().getSelected();
                        if(record) {
                            store.remove(record);
                        }
                    }
                }]
            })

        }, config);

        Portal.grids.Messages.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(params.outbox) {
            this.store.setBaseParam('mode', 'outbox');
            this.setTitle(this.initialConfig.title + ' - Wysłane');
        } else {
            this.store.setBaseParam('mode', 'inbox');
            this.setTitle(this.initialConfig.title);
        }
        this.topToolbar.moveFirst();
    }
});

Ext.reg('messages', Portal.grids.Messages);