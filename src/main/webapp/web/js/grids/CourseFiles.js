Ext.ns('Portal.grids');

Portal.grids.CourseFiles = Ext.extend(Ext.grid.EditorGridPanel, {
    constructor: function(config) {

        var store = new Ext.data.Store({
            url: 'svc/courses/{id}/files',
            reader: new Ext.data.JsonReader({
                root: 'data',
                fields: ['course', 'name',  'size', 'description']
            }),
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            autoLoad: false,
            remoteSort: true
        });

        config = Ext.apply(config || {}, {
            store: store,
            loadMask: true,
            title: 'Udostępnione pliki',
            stripeRows: true,
            viewConfig: {
                autoFill: true
            },
            clicksToEdit: 1,
            selModel: new Ext.grid.RowSelectionModel(),
            columns: [{
                header: 'Nazwa pliku',
                dataIndex: 'name',
                width: 200,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return '<a href="svc/courses/' + this.cachedId + '/files/' + record.id + '/download">' + value + '</a>';
                },
                listeners: {
                    click: function(column, grid, rowIndex, evt) {
                        var href = Ext.fly(evt.target).getAttribute('href');
                        if(Ext.isString(href)) {
                            Portal.common.Auth.setAuthorizationCookie();
                            Ext.get('portal-download-iframe').set({
                                src: href
                            });
                        }
                        evt.stopEvent();
                    }
                },
                scope: this
            },{
                header: 'Rozmiar',
                dataIndex: 'size',
                width: 60,
                renderer: function(v) {
                    var size = Number(v);
                    return size < 1048576 ? (v/1024).toFixed() + ' kB' :  (v/1048576).toFixed(2) + ' MB'
                }
            },{
                header: 'Opis',
                dataIndex: 'description',
                width: 300,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 250
                } : undefined
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Dodaj plik',
                    tooltip: 'Kliknij, aby załączyć plik do danego przedmiotu',
                    scope: this,
                    iconCls: 'upload-icon',
                    handler: function() {
                        new Portal.windows.FileUploadWindow({
                            url: 'svc/courses/' + this.cachedId + '/files',
                            onUploadSuccess: function() {
                                this.topToolbar.moveFirst();
                            },
                            enableDescription: true,
                            scope: this
                        }).show();
                    },
                    hidden: !Portal.common.Auth.isLecturer()
                },new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Usuń',
                    tooltip: 'Kliknij, aby usunąć zaznaczony plik',
                    scope: this,
                    iconCls: 'file-delete-icon',
                    handler: function() {
                        if(this.getSelectionModel().hasSelection()) {
                            store.remove(this.getSelectionModel().getSelected());
                        }
                    },
                    hidden: !Portal.common.Auth.isLecturer()
                }]
            })
        });

        Portal.grids.CourseFiles.superclass.constructor.call(this, config);
    },
    setCourseId: function(courseId) {
        this.cachedId = courseId;
        var url = 'svc/courses/' + courseId + '/files';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.topToolbar.moveFirst();
    }
});