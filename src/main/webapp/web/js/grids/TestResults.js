Ext.ns('Portal.grids');

Portal.grids.TestResults = Ext.extend(Ext.grid.EditorGridPanel, {
    constructor: function(config) {

        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/courses/{course}/tests/{test}/results',
            fields: ['id', 'student', 'group', 'assignment', {
                name: 'result',
                type: 'string'
            },{
                name: 'date',
                type: 'date',
                dateFormat: 'c'
            }],
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    var v = this.groupsCombo.getValue();
                    if(!v) {
                        return false;
                    }
                    options.params = Ext.apply(options.params || {}, {
                        group: v
                    });
                    return true;
                }
            }
        });

        this.groupsCombo = new Portal.components.GroupsComboBox({
            width: 235,
            listeners: {
                select: function() {
                    store.reload();
                }
            }
        });

        config = Ext.apply({
            store: store,
            stripeRows: true,
            loadMask: true,
            title: 'Wyniki',
            clicksToEdit: 1,
            columns: [new Ext.grid.RowNumberer(), {
                header: 'Student',
                dataIndex: 'student',
                editable: false,
                width: 300,
                renderer: function(v, meta, record) {
                    return Portal.common.Util.prepareObjectLink('user', record.data.student.id, record.data.student.label);
                }
            },{
                header: 'Numer indeksu',
                dataIndex: 'student',
                width: 200,
                renderer: function(v, meta, record) {
                    return record.data.student.indexNumber || '';
                }
            },{
                header: 'Ocena',
                dataIndex: 'result',
                width: 100,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 20
                } : false
            },{
                header: 'Data wystawienia',
                dataIndex: 'date',
                width: 100,
                renderer: function(value) {
                    return Ext.isDate(value) ? value.format('d.m.Y') : null;
                },
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'datefield',
                    maxLength: 20
                } : false
            }],
            tbar: [{
                iconCls: 'x-btn-text x-tbar-loading',
                tooltip: 'Odśwież',
                width: 18,
                handler: function() {
                    store.reload();
                }
            },'-', 'Wybierz grupę: ', this.groupsCombo]
        }, config);

        Portal.grids.TestResults.superclass.constructor.call(this, config);

        this.getSelectionModel().on('beforecellselect', function(sm, row, col) {
            return false;
        });
    },
    setCourseId: function(params) {
        var url = 'svc/courses/' + params.course + '/tests/' + params.test + '/results';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        if(Ext.isDefined(this.courseId) && this.courseId != params.course) {
            this.groupsCombo.setCourseId(params.course, true);
            this.store.loadData({
                data: [],
                total: 0
            });
        } else {
            this.groupsCombo.setCourseId(params.course, false);
            this.store.reload();
        }
        this.courseId = params.course;
    }
});