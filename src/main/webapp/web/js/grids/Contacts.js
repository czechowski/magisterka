Ext.ns('Portal.grids');

Portal.grids.Contacts = Ext.extend(Ext.grid.GridPanel, {
    constructor: function(config) {

        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/contacts',
            fields: ['id', 'label', 'email', 'firstName', 'lastName'],
            restful: true,
            autoLoad: false,
            remoteSort: true
        });

        config = Ext.apply({
            id: 'contacts',
            title: 'Kontakty',
            store: store,
            loadMask: true,
            stripeRows: true,
            viewConfig: {
                autoFill: true
            },
            iconCls: 'contacts-icon',
            columns: [new Ext.grid.RowNumberer(),{
                header: 'Użytkownik',
                width: 200,
                dataIndex: 'label',
                sortable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return Portal.common.Util.prepareObjectLink('user', record.id, record.get('label'));
                }
            },{
                header: 'Imię',
                dataIndex: 'firstName',
                width: 100,
                sortable: true
            },{
                header: 'Nazwisko',
                dataIndex: 'lastName',
                width: 100,
                sortable: true
            },{
                header: 'E-mail',
                dataIndex: 'email',
                width: 100,
                sortable: true
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    hidden: true,
                    itemId: 'separator'
                }),{
                    text: 'Dodaj do kontaktów',
                    scope: this,
                    iconCls: 'contacts-add-icon',
                    handler: function() {
                        if(this.getSelectionModel().hasSelection()) {
                            var selections = this.getSelectionModel().getSelections();
                            for(var iter = 0; iter < selections.length; iter++) {
                                Ext.Ajax.request({
                                    method: 'POST',
                                    url: 'svc/contacts/' + selections[iter].id
                                });
                            }
                        }
                    },
                    hidden: true,
                    itemId: 'addButton'
                }]
            })

        }, config);
        
        Portal.grids.Contacts.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(params.search) {
            this.store.setBaseParam('search', true);
            this.setTitle(this.initialConfig.title + ' - Szukaj');
            this.topToolbar.getComponent('separator').show();
            this.topToolbar.getComponent('addButton').show();
        } else {
            if(this.store.baseParams.search === true) {
                delete this.store.baseParams.search;
                if(this.store.lastOptions.params) {
                    delete this.store.lastOptions.params.search;
                }
            }
            this.setTitle(this.initialConfig.title);
            this.topToolbar.getComponent('separator').hide();
            this.topToolbar.getComponent('addButton').hide();
        }
        this.topToolbar.moveFirst();
    }
});

Ext.reg('contacts', Portal.grids.Contacts);