Ext.ns('Portal.grids');

Portal.grids.Courses = Ext.extend(Ext.grid.GridPanel, {
    constructor: function(config) {

        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/courses',
            fields: ['name', 'description', 'lecturer', 'unitName', 'type'],
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            autoLoad: false,
            remoteSort: true
        });

        Ext.apply(config || {}, {
            title: 'Lista przedmiotów',
            stripeRows: true,
            loadMask: true,
            viewConfig: {
                autoFill: true
            },
            iconCls: 'course-icon',
            columns: [new Ext.grid.RowNumberer(),{
                header: 'Nazwa',
                width: 200,
                dataIndex: 'name',
                sortable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return Portal.common.Util.prepareObjectLink('course', record.id, record.get('name'));
                }
            },{
                header: 'Prowadzący',
                width: 200,
                dataIndex: 'lecturer',
                sortable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    if(Ext.isObject(record.get('lecturer'))) {
                        return Portal.common.Util.prepareObjectLink('user', record.get('lecturer').id, record.get('lecturer').label);
                    } else {
                        return '';
                    }
                }
            },{
                header: 'Jednostka organizacyjna',
                width: 200,
                dataIndex: 'unitName',
                sortable: true
            },{
                header: 'Rodzaj zajęć',
                width: 200,
                dataIndex: 'type',
                sortable: true,
                renderer: function(v) {
                    switch(v) {
                        case 'LECTURE':
                            return 'Wykład';
                        case 'EXERCISES':
                            return 'Ćwiczenia';
                        case 'LABORATORY':
                            return 'Laboratorium';
                    }
                }
            }],
            store: store,
            id: 'courses',
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Dodaj przedmiot',
                    scope: this,
                    iconCls: 'course-add-icon',
                    handler: function() {
                        new Portal.windows.CourseCreateWindow({
                            store: this.store
                        });
                    },
                    hidden: !Portal.common.Auth.isLecturer()
                }]
            })
        });

        Portal.grids.Courses.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(params.search) {
            this.store.setBaseParam('search', true);
            this.setTitle(this.initialConfig.title + ' - Szukaj');
        } else {
            if(this.store.baseParams.search === true) {
                delete this.store.baseParams.search;
                if(this.store.lastOptions.params) {
                    delete this.store.lastOptions.params.search;
                }
            }
            this.setTitle(this.initialConfig.title);
        }
        this.topToolbar.moveFirst();
    }

});

Ext.reg('courses', Portal.grids.Courses);
