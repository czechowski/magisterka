Ext.ns('Portal.grids');

Portal.grids.CourseAssignments = Ext.extend(Ext.grid.EditorGridPanel, {
    constructor: function(config) {
        
        var store = new Ext.data.Store({
            url: 'svc/courses/{id}/users',
            reader: new Ext.data.JsonReader({
                root: 'data',
                fields: ['course', 'group', 'user', 'user.lastName', 'user.indexNumber', 'group.id', {
                    name: 'confirmed',
                    type: 'bool'
                }]
            }),
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            autoLoad: false,
            remoteSort: true,
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    var v = this.groupsCombo.getValue();
                    if(!v) {
                        return false;
                    }
                    options.params = Ext.apply(options.params || {}, {
                        group: v
                    });
                    return true;
                }
            }
        });

        this.groupsCombo = new Portal.components.GroupsComboBox({
            width: 235,
            emptyText: 'Wybierz grupę',
            listeners: {
                select: function() {
                    store.reload();
                }
            }
        });

        this.groupsEditor = new Portal.components.GroupsComboBox();

        config = Ext.apply(config || {}, {
            store: store,
            loadMask: true,
            title: 'Lista studentów',
            stripeRows: true,
            clicksToEdit: 1,
            viewConfig: {
                autoFill: true
            },
            columns: [new Ext.grid.RowNumberer(),{
                header: 'Student',
                dataIndex: 'user.lastName',
                width: 250,
                renderer: function(v, meta, record) {
                    return Portal.common.Util.prepareObjectLink('user', record.data.user.id, record.data.user.label);
                },
                sortable: true
            },{
                header: 'Numer indeksu',
                width: 150,
                dataIndex: 'user.indexNumber',
                sortable: true
            },{
                header: 'Grupa',
                width: 200,
                dataIndex: 'group.id',
                renderer: function(v, meta, record) {
                    return Ext.isObject(record.data.group) ? new Ext.XTemplate('<span class="groups-renderer-name">{name}</span><span class="groups-renderer-year">{year}</span>').apply(record.data.group) : '';
                },
                editor: Portal.common.Auth.isLecturer() ? this.groupsEditor : false
            },{
                header: 'Zatwierdzony',
                width: 100,
                dataIndex: 'confirmed',
                renderer: function(v) {
                    return v === true ? 'Tak' : 'Nie';
                },
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'combo',
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: ['value', 'label'],
                        data: [[true, 'Tak'], [false, 'Nie']]
                    }),
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    displayField: 'label',
                    valueField: 'value'
                } : false,
                sortable: true
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 50,
                store: store,
                items: ['-', this.groupsCombo]
            })
        });
        
        Portal.grids.CourseAssignments.superclass.constructor.call(this, config);

        this.getSelectionModel().on('beforecellselect', function(sm, row, col) {
            return false;
        });
    },
    setCourseId: function(courseId) {
        var url = 'svc/courses/' + courseId + '/users';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.groupsEditor.setCourseId(courseId, true);
        if(Ext.isDefined(this.courseId) && this.courseId != courseId) {
            this.groupsCombo.setCourseId(courseId, true);
            this.store.loadData({
                data: [],
                total: 0
            });
        } else {
            this.groupsCombo.setCourseId(courseId, false);
            this.topToolbar.moveFirst();
        }
        this.courseId = courseId;
    }
});