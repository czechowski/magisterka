Ext.ns('Portal.grids');

Portal.grids.Tests = Ext.extend(Ext.grid.EditorGridPanel, {
    constructor: function(config) {
        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/courses/{courseId}/tests',
            fields: ['id', 'name', 'type', 'date', 'description', {
                    name: 'courseId',
                    mapping: 'course.id'
            }],
            restful: true,
            writer: new Portal.common.XJsonWriter()
        });

        config = Ext.apply(config || {}, {
            store: store,
            title: 'Zaliczenia',
            stripeRows: true,
            viewConfig: {
                autoFill: true
            },
            selModel: Portal.common.Auth.isLecturer() ? new Ext.grid.CellSelectionModel() : new Ext.grid.RowSelectionModel(),
            columns: [new Ext.grid.RowNumberer(), {
                header: 'Nazwa',
                dataIndex: 'name',
                width: 200,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return new Ext.XTemplate('<a href="#test?course={courseId}&test={id}">{name}</a>').apply(record.data);
                },
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 50
                } : undefined
            },{
                header: 'Typ zaliczenia',
                dataIndex: 'type',
                width: 200,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'combo',
                    triggerAction: 'all',
                    mode: 'local',
                    store: new Ext.data.ArrayStore({
                        data: [['Egzamin', 'Egzamin'], ['Zaliczenie', 'Zaliczenie'], ['Projekt', 'Projekt']],
                        fields: ['value', 'label']
                    }),
                    displayField: 'label',
                    valueField: 'value',
                    hiddenName: 'type'
                } : undefined
            },{
                header: 'Termin',
                dataIndex: 'date',
                width: 200,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 100
                } : undefined
            },{
                header: 'Opis',
                dataIndex: 'description',
                width: 600,
                editor: Portal.common.Auth.isLecturer() ? {
                    xtype: 'textfield',
                    maxLength: 2000
                } : undefined
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Nowe zaliczenie',
                    tooltip: 'Kliknij, aby utworzyć nowe zaliczenie',
                    scope: this,
                    iconCls: 'test-add-icon',
                    hidden: !Portal.common.Auth.isLecturer(),
                    handler: function() {
                        new Portal.windows.TestCreateWindow({
                            store: store
                        });
                    }
                }]
            })
        });

        Portal.grids.Tests.superclass.constructor.call(this, config);

    },
    setCourseId: function(courseId) {
        var url = 'svc/courses/' + courseId + '/tests';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.topToolbar.moveFirst();
    }
});