Ext.ns('Portal.grids');

Portal.grids.ForumGrid = Ext.extend(Ext.grid.GridPanel, {
    constructor: function(config) {
        config = config || {};
        
        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/forum',
            fields: ['id', 'subject', 'text', 'user', 'replies', 'parent', {
                name: 'date',
                type: 'date',
                format: 'c'
            }],
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            autoLoad: config.storeAutoLoad === true
        });

        config = Ext.apply(config || {}, {
            store: store,
            viewConfig: {
                forceFit: true,
                enableRowBody: true,
                showPreview: true,
                getRowClass: this.applyRowClass
            },
            loadMask: true,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect:true
            }),
            columns: [{
                header: 'Wątek',
                dataIndex: 'subject',
                width: 500,
                renderer: this.threadRenderer
            },{
                header: 'Data',
                dataIndex: 'date',
                width: 100,
                renderer: function(value) {
                    return Ext.isDate(value) ? value.format('d.m.Y H:i') : new Date().format('d.m.Y H:i');
                }
            },{
                header: 'Odpowiedzi',
                dataIndex: 'replies',
                width: 50
            }],
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: [new Ext.Toolbar.Separator(),{
                    text: 'Nowy wątek',
                    tooltip: 'Kliknij, aby dodać nowy wątek',
                    scope: this,
                    iconCls: 'forum-add-icon',
                    handler: function() {
                        new Portal.windows.ThreadCreateWindow({
                            store: store
                        });
                    }
                },new Ext.Toolbar.Separator(),{
                    text: 'Odpowiedz',
                    tooltip: 'Kliknij, aby odpowiedziec na wybrany wątek',
                    scope: this,
                    iconCls: 'forum-reply-icon',
                    handler: config.replayHandler || this.replayHandler
                }]
            })
        });

        Portal.grids.ForumGrid.superclass.constructor.call(this, config);


    },
    setCourseId: function(courseId) {
        var url = 'svc/courses/' + courseId + '/forum';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.topToolbar.moveFirst();
    },
    applyRowClass: function(record, rowIndex, p, ds) {
        if (this.showPreview) {
            var xf = Ext.util.Format;
            p.body = '<div class="thread-cell-text">' + xf.ellipsis(xf.stripTags(record.data.text), 200) + '</div>';
            return 'x-grid3-row-expanded';
        }
        return 'x-grid3-row-collapsed';
    },
    togglePreview : function(show){
        this.view.showPreview = show;
        this.view.refresh();
    },
    threadRenderer: function(value, metaData, record, rowIndex, colIndex, store) {
        var data = record.data;
        if(data.user) {
            data.user = Portal.common.Util.prepareObjectLink('user', record.data.user.id, record.data.user.label);
        } else {
            data.user = Portal.common.Util.prepareObjectLink('user', Portal.common.Auth.getUserData('id'), Portal.common.Auth.getUserData('label'));
        }
        return new Ext.XTemplate('<div class="thread-cell-body"><div class="thread-cell-subject">{subject}</div><span class="thread-cell-user">{user}</span></div>').apply(data);
    },
    replayHandler: Ext.emptyFn
});