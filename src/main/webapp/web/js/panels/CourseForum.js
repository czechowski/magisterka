Ext.ns('Portal.panels');

Portal.panels.CourseForum = Ext.extend(Ext.Panel, {
    constructor: function(config) {
        var that = this;

        var replayHandler = function() {
            if(that.grid.getSelectionModel().hasSelection()) {
                new Portal.windows.ThreadCreateWindow({
                    store: that.preview.store,
                    parentRecord: this.getSelectionModel().getSelected()
                });
            }
        }

        this.grid = new Portal.grids.ForumGrid({
            region: 'center',
            storeAutoLoad: true,
            border: false,
            replayHandler: replayHandler
        });

        this.preview = new Portal.panels.ThreadPreview({
            border: false,
            setThreadUrl: function(parentId) {
                var url = 'svc/courses/' + that.courseId + '/forum/' + parentId + '/thread';
                this.store.proxy.setApi({
                    read    : url,
                    create  : url,
                    update  : url,
                    destroy : url
                });
                this.store.reload();
            }
        });

        config = Ext.apply({
            id: 'course-forum',
            cls: 'forum-panel course-forum',
            margins: '0 5 5 0',
            border: false,
            bodyBorder: false,
            layout: 'border',
            items: [this.grid,{
                layout: 'fit',
                items: [this.preview],
                height: 300,
                split: true,
                border: false,
                region: 'south'
            }]
        }, config);

        Portal.panels.CourseForum.superclass.constructor.call(this, config);

        this.grid.getSelectionModel().on('rowselect', function(sm, index, record){
            this.preview.setThreadUrl(record.id);
        }, this, {
            buffer:250
        });

        this.grid.mon(this.grid.store, 'load', function() {
            if(this.getSelectionModel().grid !== undefined && !this.getSelectionModel().hasSelection()) {
                this.getSelectionModel().selectFirstRow();
            }
        }, this.grid);

        this.on('activate', function() {
            if(this.grid.getSelectionModel().grid !== undefined && !this.grid.getSelectionModel().hasSelection()) {
                this.grid.getSelectionModel().selectFirstRow();
            }
        }, this);
    },
    setCourseId: function(courseId) {
        this.courseId = courseId;
        this.grid.setCourseId(courseId);
    }
});

Ext.reg('courseforum', Portal.panels.CourseForum);