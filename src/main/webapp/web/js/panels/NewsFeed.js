Ext.ns('Portal.panels');

Portal.panels.NewsFeed = Ext.extend(Ext.Panel, {
    constructor: function(config) {
        config = config || {};

        var store = new Ext.data.JsonStore({
            fields: ['content', 'course', {
                name: 'date',
                type: 'date',
                dateFormat: 'c'
            }],
            url: 'svc/newsfeed',
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            autoLoad: config.autoLoad || true,
            root: 'data'
        });

        var tpl = new Ext.XTemplate('<tpl for=".">',
            '<div class="news-feed-wrap">',
            '<div class="news-feed-header">',
            '<span class="news-feed-date">{date}</span>',
            config.disableFeedsTitles === true ? '' : ' - <a class="news-feed-title" href="#course?id={courseId}">{courseName}</a>',
            '</div><div class="news-feed-content">{content}</div>',
            '</div>',
            '</tpl>');

        this.dataView = new Ext.DataView({
            store: store,
            autoHeight:true,
            tpl: config.tpl || tpl,
            itemSelector: 'div.news-feed-wrap',
            prepareData: function(data, recordIndex, record) {
                return {
                    content: data.content,
                    date: Ext.isDate(data.date) ? data.date.format('d.m.Y H:i') : new Date().format('d.m.Y H:i'),
                    courseId: data.course ? data.course.id : '',
                    courseName: data.course ? data.course.name : 'Zapisywanie...'
                }
            }
        });

        var tbarItems = [];

        if(config.dashboardMode !== true) {
            tbarItems.push(new Ext.Toolbar.Separator({
                hidden: !Portal.common.Auth.isLecturer()
            }),{
                text: 'Dodaj ogłoszenie',
                tooltip: 'Kliknij, aby dodać ogłoszenie dla studentów',
                scope: this,
                iconCls: 'feed-add-icon',
                handler: function() {
                    new Portal.windows.NewsFeedCreateWindow({
                        store: store
                    });
                },
                hidden: !Portal.common.Auth.isLecturer()
            });
        }

        config = Ext.apply({
            title: 'Ogłoszenia',
            items: [this.dataView],
            frame: false,
            layout: 'fit',
            autoScroll: true,
            padding: '0 20px 0 0',
            tbar: new Ext.PagingToolbar({
                pageSize: 30,
                store: store,
                items: tbarItems
            })
        }, config);

        Portal.panels.NewsFeed.superclass.constructor.call(this, config);
        
        this.on('afterrender', function() {
            this.loadMask = new Ext.LoadMask(this.bwrap, {
                store: store
            });
        }, this);
    },
    setCourseId: function(courseId) {
        var url = 'svc/courses/' + courseId + '/newsfeed';
        this.dataView.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.dataView.store.reload();
    }
});

Ext.reg('newsfeed', Portal.panels.NewsFeed);