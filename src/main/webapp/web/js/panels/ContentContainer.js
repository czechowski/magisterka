Ext.ns('Portal.panels');

Portal.panels.ContentContainer = Ext.extend(Ext.Container, {
    constructor: function(config) {

        Ext.apply(config || {}, {
            layout: 'card',
            id: 'contentcontainer',
            defaults: {
                frame: false
            }
        });

        Portal.panels.ContentContainer.superclass.constructor.call(this, config);
    },
    setContent: function(id, params) {
        id = Ext.ComponentMgr.isRegistered(id) ? id : (Portal.common.Auth.isLecturer() ? 'courses' : 'dashboard');
        if(this.getComponent(id)) {
            this.layout.setActiveItem(id);
        } else {
            this.layout.setActiveItem(this.add({
                xtype: id
            }));
        }
        if(this.layout.activeItem.title) {
            document.title = 'Portal - ' + this.layout.activeItem.title;
        } else {
            document.title = 'Portal';
        }
        if(Ext.isFunction(this.layout.activeItem.onHistoryChange)) {
            this.layout.activeItem.onHistoryChange(params);
        }
    }
});

Ext.reg('contentcontainer', Portal.panels.ContentContainer);
