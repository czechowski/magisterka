Ext.ns('Portal.panels');

Portal.panels.Test = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        this.formPanel = new Ext.form.FormPanel({
            labelAlign: 'left',
            border: false,
            frame: false,
            labelWidth: 150,
            padding: 15,
            defaults: {
                anchor: '100%',
                xtype: Portal.common.Auth.isLecturer() ? 'textfield' : 'displayfield',
                boxMaxWidth: 500
            },
            cls: 'displayfield-new-look',
            items: [{
                fieldLabel: 'Przedmiot',
                name: 'course',
                xtype: 'displayfield'
            },{
                fieldLabel: 'Nazwa',
                name: 'name'
            },{
                fieldLabel: 'Typ zaliczenia',
                name: 'type',
                xtype: Portal.common.Auth.isLecturer() ? 'combo' : 'displayfield',
                triggerAction: 'all',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    data: [['Egzamin', 'Egzamin'], ['Zaliczenie', 'Zaliczenie'], ['Projekt', 'Projekt']],
                    fields: ['value', 'label']
                }),
                displayField: 'label',
                valueField: 'value',
                hiddenName: 'type'
            },{
                fieldLabel: 'Termin',
                name: 'date'
            }]
        });

        this.testResults = new Portal.grids.TestResults();

        this.description = new Portal.panels.TestDescriptionPanel({
            that: this
        });

        this.tabPanel = new Ext.TabPanel({
            items: [this.description, this.testResults],
            activeTab: 0,
            border: false,
            flex: 1
        });

        config = Ext.apply({
            id: 'test',
            cls: 'test-panel',
            iconCls: 'test-icon',
            title: 'Strona zaliczenia',
            items: [this.formPanel, this.tabPanel],
            layout: 'vbox',
            layoutConfig: {
                align : 'stretch',
                pack  : 'start'
            },
            tbar: [{
                xtype: 'xbuttongroup',
                items: [{
                    text: 'Odśwież',
                    iconCls: 'x-tbar-loading',
                    handler: function() {
                        if(this.cachedParams) {
                            this.loadRecord(this.cachedParams);
                        }
                    },
                    scope: this
                },new Ext.Toolbar.Separator({
                    hidden: !Portal.common.Auth.isLecturer()
                }),{
                    text: 'Zapisz',
                    hidden: !Portal.common.Auth.isLecturer(),
                    iconCls: 'save-icon',
                    handler: this.onSave,
                    scope: this
                }]
            }]
        }, config);

        Portal.panels.Test.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(Ext.isNumber(Number(params.test))) {
            this.cachedParams = params;
            this.loadRecord(params);
            this.testResults.setCourseId(params);
        } else {
            this.disable();
        }
    },
    loadRecord: function(params) {
        this.bwrap.mask('Wczytywanie danych...', 'x-mask-loading');
        Ext.Ajax.request({
            url: 'svc/courses/' + params.course + '/tests/' + params.test,
            method: 'GET',
            success: function(response) {
                var jsonResponse = Ext.decode(response.responseText);
                if(Ext.isArray(jsonResponse.data) && jsonResponse.data.length > 0) {
                    if(this.disabled) {
                        this.enable();
                    }
                    this.onLoadSuccess(jsonResponse.data[0]);
                } else {
                    this.onLoadFailure();
                }
            },
            failure: this.onLoadFailure,
            scope: this
        });
    },
    onLoadSuccess: function(data) {
        this.bwrap.unmask();
        this.cachedData = data;
        for(var key in data) {
            this.setFieldValue(key, data[key], data.type);
        }
    },
    onLoadFailure: function() {
        this.bwrap.unmask();
        this.disable();
    },
    setFieldValue: function(name, value, type) {
        if(name === 'description') {
            this.description.items.get(0).setValue(value);
        } else {
            var field = this.formPanel.getForm().findField(name);
            if(field) {
                if(name === 'course') {
                    value = Portal.common.Util.prepareObjectLink('course', value.id, value.name);
                }
                field.setValue(value);
            }
        }
    },
    updateRecord: function(data) {
        Ext.Ajax.request({
            url: 'svc/courses/' + this.cachedParams.course + '/tests/' + this.cachedParams.test,
            method: 'PUT',
            jsonData: data,
            scope: this
        });
    },
    onSave: function() {
        var data = {}, dirty = false;
        for(var iter = 0; iter < this.formPanel.getForm().items.length; iter++) {
            var field = this.formPanel.getForm().items.get(iter);
            if(field.xtype !== 'displayfield') {
                if(field.getValue() !== this.cachedData[field.name]) {
                    data[field.name] = field.getValue();
                    dirty = true;
                }
            }
        }
        if(this.description.items.get(0).getValue() !== this.cachedData.description) {
            data.description = this.description.items.get(0).getValue();
            dirty = true;
        }
        if(dirty) {
            this.updateRecord(data);
        }
    }
});

Ext.reg('test', Portal.panels.Test);

Portal.panels.TestDescriptionPanel = Ext.extend(Ext.form.FormPanel, {
    constructor: function(config) {

        var items = [Portal.common.Auth.isLecturer() ? {
            xtype: 'htmleditor',
            anchor: '100% 100%',
            hideLabel: true,
            validator: function(value) {
                if(value.length > 2000) {
                    return 'Długość opisu jest zbyt duża (obecnie: ' + value.length + ' znaków, max: 10000)';
                }
                return true;
            },
            preventMark: false,
            getErrors: Ext.form.TextArea.prototype.getErrors
        } : {
            xtype: 'displayfield',
            anchor: '100% 100%',
            autoScroll: true,
            hideLabel: true
        }];

        config = Ext.apply({
            title: 'Opis',
            frame: false,
            padding: 5,
            cls: 'displayfield-new-look',
            items: items,
            labelAlign: 'left'
        }, config);

        Portal.panels.TestDescriptionPanel.superclass.constructor.call(this, config);
    }
});