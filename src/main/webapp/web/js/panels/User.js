Ext.ns('Portal.panels');

Portal.panels.User = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        this.formPanel = new Ext.form.FormPanel({
            defaults: {
                anchor: '100%',
                xtype: 'textfield',
                boxMaxWidth: 500
            },
            cls: 'displayfield-new-look',
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'E-mail',
                name: 'email'
            },{
                fieldLabel: 'Imię',
                name: 'firstName'
            },{
                fieldLabel: 'Nazwisko',
                name: 'lastName'
            },{
                fieldLabel: 'Etykieta',
                name: 'label'
            },{
                fieldLabel: 'Tytuł naukowy',
                name: 'title',
                hidden: true
            },{
                fieldLabel: 'Numer indeksu',
                name: 'indexNumber',
                hidden: true
            }]
        });

        this.readOnlyFormPanel = new Ext.form.FormPanel({
            defaults: {
                anchor: '100%',
                xtype: 'displayfield',
                boxMaxWidth: 500
            },
            cls: 'displayfield-new-look',
            items: [{
                fieldLabel: 'E-mail',
                name: 'email'
            },{
                fieldLabel: 'Imię',
                name: 'firstName'
            },{
                fieldLabel: 'Nazwisko',
                name: 'lastName'
            },{
                fieldLabel: 'Etykieta',
                name: 'label'
            },{
                fieldLabel: 'Tytuł naukowy',
                name: 'title',
                hidden: true
            },{
                fieldLabel: 'Numer indeksu',
                name: 'indexNumber',
                hidden: true
            }]
        });

        config = Ext.apply({
            id: 'user',
            cls: 'user-panel',
            iconCls: 'user-icon',
            title: 'Strona użytkownika',
            items: [this.readOnlyFormPanel, this.formPanel],
            layout: 'card',
            activeItem: 0,
            defaults: {
                labelAlign: 'left',
                border: false,
                frame: false,
                labelWidth: 150,
                padding: 15
            },
            tbar: [{
                xtype: 'xbuttongroup',
                items: [{
                    text: 'Odśwież',
                    iconCls: 'x-tbar-loading',
                    handler: function() {
                        if(this.cachedId) {
                            this.loadRecord(this.cachedId);
                        }
                    },
                    scope: this
                },new Ext.Toolbar.Separator({
                    ref: '../../separator'
                }),{
                    text: 'Zapisz',
                    iconCls: 'save-icon',
                    handler: this.onSave,
                    scope: this,
                    ref: '../../saveButton'
                },new Ext.Toolbar.Separator({
                    ref: '../../separator2'
                }),{
                    text: 'Wyślij wiadomość',
                    iconCls: 'messages-icon-add',
                    handler: function() {
                        var values = {
                            receiver: this.cachedData.email || ''
                        }
                        new Portal.windows.MessageCreateWindow({
                            values: values
                        }).show();
                    },
                    scope: this,
                    ref: '../../sendMessage'
                },new Ext.Toolbar.Separator({
                    ref: '../../separator3'
                }),{
                    text: 'Dodaj do kontaktów',
                    iconCls: 'contacts-add-icon',
                    handler: function() {
                        Ext.Ajax.request({
                            method: 'POST',
                            url: 'svc/contacts/' + this.cachedData.id
                        });
                    },
                    scope: this,
                    ref: '../../addContact'
                }]
            }]
        }, config);

        Portal.panels.User.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(Ext.isNumber(Number(params.id))) {
            this.cachedId = params.id;
            this.loadRecord(params.id);
        } else {
            this.disable();
        }
    },
    loadRecord: function(id) {
        this.setTitle("Strona użytkownika: ładowanie...");
        this.bwrap.mask('Wczytywanie danych...', 'x-mask-loading');
        Ext.Ajax.request({
            url: 'svc/users/' + id,
            method: 'GET',
            success: function(response) {
                var jsonResponse = Ext.decode(response.responseText);
                if(Ext.isArray(jsonResponse.data) && jsonResponse.data.length > 0) {
                    if(this.disabled) {
                        this.enable();
                    }
                    this.onLoadSuccess(jsonResponse.data[0]);
                } else {
                    this.onLoadFailure();
                }
            },
            failure: this.onLoadFailure,
            scope: this
        });
    },
    onLoadSuccess: function(data) {
        this.bwrap.unmask();
        this.cachedData = data;
        if(data.id === Portal.common.Auth.getUserData('id')) {
            this.layout.setActiveItem(1);
            this.separator.show();
            this.saveButton.show();
            this.separator2.hide();
            this.sendMessage.hide();
            this.separator3.hide();
            this.addContact.hide();
        } else {
            this.layout.setActiveItem(0);
            this.separator.hide();
            this.saveButton.hide();
            this.separator2.show();
            this.sendMessage.show();
            this.separator3.show();
            this.addContact.show();
        }
        for(var key in data) {
            if(key === 'label') {
                this.setTitle("Strona użytkownika: " + data[key]);
            }
            this.setFieldValue(key, data[key], data.type);
        }
    },
    onLoadFailure: function() {
        this.bwrap.unmask();
        this.disable();
    },
    setFieldValue: function(name, value, type) {
        var field = this.layout.activeItem.getForm().findField(name);
        if(name === 'title') {
            field.setVisible(type === 'LECTURER');
        }
        if(name === 'indexNumber') {
            field.setVisible(type === 'STUDENT');
        }
        if(field) {
            field.setValue(value);
        }
    },
    updateRecord: function(data) {
        Ext.Ajax.request({
            url: 'svc/users/' + this.cachedId,
            method: 'PUT',
            jsonData: data,
            scope: this
        });
    },
    onSave: function() {
        var data = {}, dirty = false;
        for(var iter = 0; iter < this.layout.activeItem.getForm().items.length; iter++) {
            var field = this.layout.activeItem.getForm().items.get(iter);
            if(field.xtype === 'textfield') {
                if(field.getValue() !== this.cachedData[field.name]) {
                    data[field.name] = field.getValue();
                    dirty = true;
                }
            }
        }
        if(dirty) {
            this.updateRecord(data);
        }
    }
});

Ext.reg('user', Portal.panels.User);