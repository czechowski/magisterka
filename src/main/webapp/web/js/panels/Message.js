Ext.ns('Portal.panels');

Portal.panels.Message = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        this.formPanel = new Ext.form.FormPanel({
            defaults: {
                anchor: '100%',
                xtype: 'displayfield',
                boxMaxWidth: 500
            },
            labelAlign: 'left',
            border: false,
            frame: false,
            labelWidth: 150,
            padding: 15,
            cls: 'displayfield-new-look',
            items: [{
                fieldLabel: 'Nadawca',
                name: 'sender'
            },{
                fieldLabel: 'Odbiorca',
                name: 'receiver'
            },{
                fieldLabel: 'Data',
                name: 'date'
            },{
                fieldLabel: 'Temat',
                name: 'subject'
            },{
                fieldLabel: 'Treść wiadomości',
                name: 'text',
                anchor: '100%',
                height: 300,
                boxMaxWidth: 1000
            }]
        });

        config = Ext.apply({
            id: 'message',
            cls: 'message-panel',
            iconCls: 'messages-icon',
            title: 'Wiadomość',
            items: [this.formPanel],
            autoScroll: true,
            tbar: [{
                xtype: 'buttongroup',
                items: [{
                    text: 'Odśwież',
                    iconCls: 'x-tbar-loading',
                    handler: function() {
                        if(this.cachedId) {
                            this.loadRecord(this.cachedId);
                        }
                    },
                    scope: this
                },new Ext.Toolbar.Separator(),{
                    text: 'Utwórz',
                    tooltip: 'Utwórz wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-add',
                    handler: function() {
                        new Portal.windows.MessageCreateWindow().show();
                    }
                },new Ext.Toolbar.Separator(),{
                    text: 'Odpowiedz',
                    tooltip: 'Odpowiedz na wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-reply',
                    handler: function() {
                        var values = {
                            receiver: this.cachedData.sender.email || '',
                            subject: 'Re: ' + this.cachedData.subject
                        }
                        new Portal.windows.MessageCreateWindow({
                            values: values
                        }).show();
                    }
                },new Ext.Toolbar.Separator(),{
                    text: 'Prześlij',
                    tooltip: 'Prześlij dalej wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-forward',
                    handler: function() {
                        var values = {
                            text: this.cachedData.text,
                            subject: 'Fwd: ' + this.cachedData.subject
                        }
                        new Portal.windows.MessageCreateWindow({
                            values: values
                        }).show();
                    }
                },new Ext.Toolbar.Separator(),{
                    text: 'Usuń',
                    tooltip: 'Usuń wiadomość',
                    scope: this,
                    iconCls: 'messages-icon-delete',
                    handler: function() {
                        Ext.Ajax.request({
                            method: 'DELETE',
                            url: 'svc/messages/' + this.cachedId,
                            success: function() {
                                Ext.History.add('messages?inbox');
                            }
                        });
                    }
                }]
            }]
        }, config);

        Portal.panels.Message.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(Ext.isNumber(Number(params.id))) {
            this.cachedId = params.id;
            this.loadRecord(params.id);
        } else {
            this.disable();
        }
    },
    loadRecord: function(id) {
        Ext.Ajax.request({
            url: 'svc/messages/' + id,
            method: 'GET',
            success: function(response) {
                var jsonResponse = Ext.decode(response.responseText);
                if(Ext.isArray(jsonResponse.data) && jsonResponse.data.length > 0) {
                    if(this.disabled) {
                        this.enable();
                    }
                    this.onLoadSuccess(jsonResponse.data[0]);
                } else {
                    this.onLoadFailure();
                }
            },
            failure: this.onLoadFailure,
            scope: this
        });
    },
    onLoadSuccess: function(data) {
        this.cachedData = data;
        if(data.read !== true) {
            this.updateRecord({
                read: true
            });
        }
        data.read = true;
        for(var key in data) {
            this.setFieldValue(key, data[key], data.type);
        }
    },
    onLoadFailure: function() {
        this.disable();
    },
    setFieldValue: function(name, value, type) {
        var field = this.formPanel.getForm().findField(name);
        if(field) {
            if(name === 'sender' || name === 'receiver') {
                value = Portal.common.Util.prepareObjectLink('user', value.id, value.label);
            }
            if(name === 'date') {
                var date = Date.parseDate(value, 'c');
                value = Ext.isDate(date) ? date.format('d.m.Y H:i') : '';
            }
            if(name === 'subject') {
                value = value || '(brak tematu)'
            }
            field.setValue(value);
        }
    },
    updateRecord: function(data) {
        Ext.Ajax.request({
            url: 'svc/messages/' + this.cachedId,
            method: 'PUT',
            jsonData: data,
            scope: this
        });
    }
});

Ext.reg('message', Portal.panels.Message);