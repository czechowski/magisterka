Ext.ns('Portal.panels');

Portal.panels.Dashboard = Ext.extend(Portal.panels.NewsFeed, {
    title: 'Aktualności',
    id: 'dashboard',
    iconCls: 'dashboard-icon',
    dashboardMode: true
});

Ext.reg('dashboard', Portal.panels.Dashboard);