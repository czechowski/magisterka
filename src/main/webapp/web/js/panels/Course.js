Ext.ns('Portal.panels');

Portal.panels.Course = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        this.formPanel = new Portal.panels.CourseContentPanel({
            that: this
        });
        
        this.news = new Portal.panels.NewsFeed({
            title: 'Ogłoszenia',
            autoLoad: false,
            border: false,
            disableFeedsTitles: true
        });

        this.students = new Portal.grids.CourseAssignments();

        this.groups = new Portal.grids.CourseGroups();

        this.files = new Portal.grids.CourseFiles();

        this.tests = new Portal.grids.Tests();

        this.questionnaires = new Portal.grids.Questionnaires();

        this.forum = new Portal.panels.CourseForum({
            title: 'Forum przedmiotu'
        });

        this.tabPanel = new Ext.TabPanel({
            items: [this.formPanel, this.news, this.forum, this.files, this.students, this.groups, this.tests, this.questionnaires],
            activeTab: 0,
            border: false,
            flex: 1
        });

        config = Ext.apply({
            id: 'course',
            cls: 'course-panel',
            iconCls: 'course-icon',
            title: 'Strona przedmiotu',
            items: [this.tabPanel],
            layout: 'fit'
        }, config);

        Portal.panels.Course.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(Ext.isNumber(Number(params.id))) {
            this.cachedId = params.id;
            this.loadRecord(params.id);
            this.students.setCourseId(params.id);
            this.groups.setCourseId(params.id);
            this.files.setCourseId(params.id);
            this.news.setCourseId(params.id);
            this.tests.setCourseId(params.id);
            this.forum.setCourseId(params.id);
            this.questionnaires.setCourseId(params.id);
        } else {
            this.disable();
        }
    },
    loadRecord: function(id) {
        this.setTitle('Strona przedmiotu: ładowanie...');
        this.bwrap.mask('Wczytywanie danych...', 'x-mask-loading');
        Ext.Ajax.request({
            url: 'svc/courses/' + id,
            method: 'GET',
            success: function(response) {
                var jsonResponse = Ext.decode(response.responseText);
                if(Ext.isArray(jsonResponse.data) && jsonResponse.data.length > 0) {
                    if(this.disabled) {
                        this.enable();
                    }
                    this.onLoadSuccess(jsonResponse.data[0]);
                } else {
                    this.onLoadFailure();
                }
            },
            failure: this.onLoadFailure,
            scope: this
        });
    },
    onLoadSuccess: function(data) {
        this.bwrap.unmask();
        this.cachedData = data;
        for(var key in data) {
            this.setFieldValue(key, data[key]);
            if(key === 'name') {
                this.setTitle('Strona przedmiotu: ' + data[key]);
            }
        }
    },
    onLoadFailure: function() {
        this.bwrap.unmask();
        this.disable();
    },
    setFieldValue: function(name, value) {
        var field = this.formPanel.getForm().findField(name);
        if(field) {
            if(name === 'lecturer') {
                value = Portal.common.Util.prepareObjectLink('user', value.id, value.label);
            }
            field.setValue(value);
        }
    },
    updateRecord: function(data) {
        Ext.Ajax.request({
            url: 'svc/courses/' + this.cachedId,
            method: 'PUT',
            jsonData: data,
            scope: this,
            success: function(response) {
                var jsonResponse = Ext.decode(response.responseText);
                if(Ext.isArray(jsonResponse.data) && jsonResponse.data.length > 0) {
                    this.cachedData = jsonResponse.data;
                }
            }
        });
    },
    onSave: function() {
        var data = {}, dirty = false;
        for(var iter = 0; iter < this.formPanel.getForm().items.length; iter++) {
            var field = this.formPanel.getForm().items.get(iter);
            if(field.xtype === 'textfield' || field.xtype === 'htmleditor') {
                if(field.getValue() !== this.cachedData[field.name]) {
                    data[field.name] = field.getValue();
                    dirty = true;
                }
            }
        }
        if(dirty) {
            this.updateRecord(data);
        }
    }
});

Ext.reg('course', Portal.panels.Course);

Portal.panels.CourseContentPanel = Ext.extend(Ext.form.FormPanel, {
    constructor: function(config) {

        var that = config.that;

        var items = [{
            fieldLabel: 'Nazwa',
            name: 'name'
        },{
            fieldLabel: 'Jednostka organizacyjna',
            name: 'unitName'
        },{
            fieldLabel: 'Rodzaj zajęć',
            name: 'type',
            xtype: Portal.common.Auth.isLecturer() ? 'combo' : 'displayfield',
            triggerAction: 'all',
            mode: 'local',
            store: new Ext.data.ArrayStore({
                data: [['LECTURE', 'Wykład'], ['EXERCISES', 'Ćwiczenia'], ['LABORATORY', 'Laboratorium']],
                fields: ['value', 'label']
            }),
            displayField: 'label',
            valueField: 'value',
            hiddenName: 'type',
            setRawValue: Portal.common.Auth.isLecturer() ? Ext.form.ComboBox.prototype.setRawValue : function(v) {
                switch(v) {
                    case 'LECTURE':
                        v = 'Wykład';
                        break;
                    case 'EXERCISES':
                        v = 'Ćwiczenia';
                        break;
                    case 'LABORATORY':
                        v = 'Laboratorium';
                        break;
                }
                Ext.form.DisplayField.prototype.setRawValue.call(this, v);
            }
        },{
            xtype: 'displayfield',
            fieldLabel: 'Prowadzący',
            name: 'lecturer'
        }, Portal.common.Auth.isLecturer() ? {
            fieldLabel: 'Opis',
            name: 'description',
            xtype: 'htmleditor',
            anchor: '100% -92',
            validator: function(value) {
                if(value.length > 10000) {
                    return 'Długość opisu jest zbyt duża (obecnie: ' + value.length + ' znaków, max: 10000)';
                }
                return true;
            },
            preventMark: false,
            getErrors: Ext.form.TextArea.prototype.getErrors
        } : {
            fieldLabel: 'Opis',
            xtype: 'displayfield',
            name: 'description',
            anchor: '100% -92'
        }];

        config = Ext.apply({
            title: 'Opis',
            frame: false,
            padding: '15px 15px 10px 15px',
            labelAlign: 'left',
            border: false,
            labelWidth: 150,
            cls: 'displayfield-new-look',
            items: items,
            autoScroll: true,
            defaults: {
                anchor: '100%',
                xtype: Portal.common.Auth.isLecturer() ? 'textfield' : 'displayfield'
            },
            tbar: [{
                text: 'Odśwież',
                iconCls: 'x-tbar-loading',
                handler: function() {
                    if(that.cachedId) {
                        that.loadRecord(that.cachedId);
                    }
                },
                scope: that
            },new Ext.Toolbar.Separator({
                hidden: !Portal.common.Auth.isLecturer()
            }),{
                text: 'Zapisz',
                iconCls: 'save-icon',
                hidden: !Portal.common.Auth.isLecturer(),
                handler: that.onSave,
                scope: that
            },new Ext.Toolbar.Separator({
                hidden: !Portal.common.Auth.isLecturer()
            }),{
                text: 'Dodaj ogłoszenie',
                iconCls: 'feed-add-icon',
                tooltip: 'Kliknij, aby dodać ogłoszenie dla studentów',
                hidden: !Portal.common.Auth.isLecturer(),
                handler: function() {
                    new Portal.windows.NewsFeedCreateWindow({
                        store: that.news.dataView.store
                    });
                },
                scope: that
            },new Ext.Toolbar.Separator({
                hidden: !Portal.common.Auth.isLecturer()
            }),{
                text: 'Dodaj plik',
                iconCls: 'upload-icon',
                hidden: !Portal.common.Auth.isLecturer(),
                handler: function() {
                    new Portal.windows.FileUploadWindow({
                        url: 'svc/courses/' + that.cachedId + '/files',
                        onUploadSuccess: function() {
                            that.files.topToolbar.moveFirst();
                        },
                        enableDescription: true,
                        scope: that
                    }).show();
                },
                scope: that
            },new Ext.Toolbar.Separator({
                hidden: !Portal.common.Auth.isLecturer()
            }),{
                text: 'Dodaj grupę',
                tooltip: 'Kliknij, aby dodać grupę lektorską',
                scope: that,
                iconCls: 'group-add-icon',
                hidden: !Portal.common.Auth.isLecturer(),
                handler: function() {
                    new Portal.windows.GroupCreateWindow({
                        store: that.groups.store
                    });
                }
            },new Ext.Toolbar.Separator({
                hidden: !Portal.common.Auth.isLecturer()
            }),{
                text: 'Nowe zaliczenie',
                tooltip: 'Kliknij, aby utworzyć zaliczenie',
                scope: that,
                iconCls: 'test-add-icon',
                hidden: !Portal.common.Auth.isLecturer(),
                handler: function() {
                    new Portal.windows.TestCreateWindow({
                        store: that.tests.store
                    });
                }
            }]
        }, config);

        Portal.panels.CourseContentPanel.superclass.constructor.call(this, config);
    }
});
