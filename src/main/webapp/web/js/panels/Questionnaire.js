Ext.ns('Portal.panels');

Portal.panels.Questionnaire = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        this.chartContainer = new Ext.Panel({
            plain: true,
            flex: 1,
            border: false
        });

        this.formPanel = new Ext.form.FormPanel({
            labelAlign: 'top',
            buttonAlign: 'left',
            border: false,
            frame: false,
            monitorValid: true,
            labelWidth: 150,
            padding: 15,
            cls: 'displayfield-new-look',
            items: [],
            flex: 1,
            listeners: {
                clientvalidation: function(form, valid) {
                    if(form.getComponent('sendAnswer')) {
                        form.getComponent('sendAnswer').setDisabled(!valid);
                    }
                },
                scope: this
            }
        });

        config = Ext.apply({
            title: 'Ankieta',
            cls: 'questionnaire-panel',
            layout: 'hbox',
            layoutConfig: {
                align: 'stretch'
            },
            items: [this.formPanel, this.chartContainer]
        }, config);

        Portal.panels.Questionnaire.superclass.constructor.call(this, config);
    },
    onHistoryChange: function(params) {
        if(Ext.isNumber(Number(params.questionnaire))) {
            this.cachedParams = params;
            this.loadRecord(params);
        } else {
            this.disable();
        }
    },
    loadRecord: function(params) {
        this.bwrap.mask('Wczytywanie danych...', 'x-mask-loading');
        Ext.Ajax.request({
            url: 'svc/courses/' + params.course + '/questionnaires/' + params.questionnaire,
            method: 'GET',
            success: function(response) {
                var jsonResponse = Ext.decode(response.responseText);
                if(Ext.isArray(jsonResponse.data) && jsonResponse.data.length > 0) {
                    if(this.disabled) {
                        this.enable();
                    }
                    this.onLoadSuccess(jsonResponse.data[0]);
                } else {
                    this.onLoadFailure();
                }
            },
            failure: this.onLoadFailure,
            scope: this
        });
    },
    onLoadSuccess: function(data) {
        this.bwrap.unmask();
        this.cachedData = data;
        this.loadChart(data);
        this.loadForm(data);
        for(var key in data) {
            this.setFieldValue(key, data[key], data.type);
        }
    },
    onLoadFailure: function() {
        this.bwrap.unmask();
        this.disable();
    },
    setFieldValue: function(name, value, type) {
        
    },
    loadChart: function(data) {
        this.chartStore = new Ext.data.JsonStore({
            fields: ['answer', 'total'],
            root: 'entries',
            data: data
        });

        var chart = new Ext.chart.PieChart({
            store: this.chartStore,
            dataField: 'total',
            flex: 1,
            categoryField: 'answer',
            extraStyle: {
                legend: {
                    display: 'top',
                    padding: 15,
                    font: {
                        family: 'Tahoma',
                        size: 13
                    }
                }
            }
        });

        this.chartContainer.removeAll();
        this.chartContainer.add(chart);
        this.chartContainer.doLayout();
        this.doLayout();
    },
    loadForm: function(data) {
        var items = [];
        for(var iter = 0; iter < data.entries.length; iter++) {
            items.push({
                boxLabel: data.entries[iter].answer,
                name: data.id,
                inputValue: data.entries[iter].id
            });
        }
        this.formPanel.removeAll();
        this.formPanel.add({
            xtype: 'radiogroup',
            fieldLabel: data.question,
            columns: 1,
            items: items,
            allowBlank: false
        },{
                xtype: 'button',
                itemId: 'sendAnswer',
                disabled: true,
                text: 'Wyślij odpowiedź',
                handler: function() {
                    if(this.formPanel.items.get(0).getValue()) {
                        this.answer(this.formPanel.items.get(0).getValue().inputValue);
                    }
                },
                scope: this
            });
        this.doLayout();
    },
    answer: function(answerId) {
        Ext.Ajax.request({
            url: 'svc/courses/' + this.cachedParams.course + '/questionnaires/' + this.cachedParams.questionnaire + '/answer/' + answerId,
            method: 'PUT',
            scope: this,
            success: function() {
                var data = this.cachedData;
                for(var iter = 0; iter < data.entries.length; iter++) {
                    if(data.entries[iter].id === answerId) {
                        data.entries[iter].total++;

                    }
                }
                this.chartStore.loadData(data);
            }
        });
    }
});

Ext.reg('questionnaire', Portal.panels.Questionnaire);