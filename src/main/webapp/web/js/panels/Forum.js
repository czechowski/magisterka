Ext.ns('Portal.panels');

Portal.panels.Forum = Ext.extend(Ext.TabPanel, {
    constructor: function(config) {
        var that = this;

        var replayHandler = function() {
            if(that.grid.getSelectionModel().hasSelection()) {
                new Portal.windows.ThreadCreateWindow({
                    store: that.preview.store,
                    parentRecord: this.getSelectionModel().getSelected()
                });
            }
        }

        this.grid = new Portal.grids.ForumGrid({
            region: 'center',
            storeAutoLoad: true,
            replayHandler: replayHandler
        });


        this.grid.mon(this.grid.store, 'load', function() {
            if(!this.getSelectionModel().hasSelection()) {
                this.getSelectionModel().selectFirstRow();
            }
        }, this.grid);

        this.preview = new Portal.panels.ThreadPreview();

        config = Ext.apply({
            id: 'forum',
            cls: 'forum-panel',
            activeTab: 0,
            margins: '0 5 5 0',
            resizeTabs: true,
            tabWidth: 150,
            minTabWidth: 120,
            enableTabScroll: true,
            border: true,
            items: [{
                layout: 'border',
                hideMode:'offsets',
                title: 'Forum',
                iconCls: 'forum-icon',
                items: [this.grid,{
                    layout: 'fit',
                    items: [this.preview],
                    height: 300,
                    split: true,
                    border: false,
                    region: 'south'
                }]
            }]
        }, config);

        Portal.panels.Forum.superclass.constructor.call(this, config);

        this.grid.getSelectionModel().on('rowselect', function(sm, index, record){
            this.preview.setThreadUrl(record.id);
        }, this, {buffer:250});
    }
});

Ext.reg('forum', Portal.panels.Forum);