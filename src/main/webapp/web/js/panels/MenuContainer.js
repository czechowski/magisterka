Ext.ns('Portal.panels');

Portal.panels.MenuContainer = Ext.extend(Ext.tree.TreePanel, {
    constructor: function(config) {

        var children = [];

        if(!Portal.common.Auth.isLecturer()) {
            children.push({
                text: 'Aktualności',
                leaf: true,
                iconCls: 'dashboard-icon',
                href: '#dashboard'
            });
        }

        children.push({
            text: 'Przedmioty',
            leaf: false,
            href: '#courses',
            iconCls: 'course-icon',
            children: [{
                text: 'Szukaj',
                href: '#courses?search',
                leaf: true,
                allowSelect: false,
                cls: 'node-icon-hidden'
            }]
        },{
            text: 'Wiadomości',
            leaf: false,
            href: '#messages?inbox',
            iconCls: 'messages-icon',
            children: [{
                text: 'Wysłane',
                href: '#messages?outbox',
                leaf: true,
                allowSelect: false,
                cls: 'node-icon-hidden'
            }]
        },{
            text: 'Kontakty',
            leaf: false,
            href: '#contacts',
            iconCls: 'contacts-icon',
            children: [{
                text: 'Szukaj',
                href: '#contacts?search',
                leaf: true,
                allowSelect: false,
                cls: 'node-icon-hidden'
            }]
        },{
            text: 'Forum',
            leaf: true,
            href: '#forum',
            iconCls: 'forum-icon'
        });

        var root = new Ext.tree.AsyncTreeNode({
            children: children,
            expanded: true,
            href: ''
        });

        Ext.apply(config, {
            lines: false,
            header: true,
            frame: false,
            title: 'Menu',
            autoScroll: true,
            rootVisible: false,
            root: root,
            useArrows: true,
            singleExpand: true,
            animate: false,
            id: 'menucontainer',
            cls: 'menu-container',
            iconCls: 'menu-panel-icon',
            listeners: {
                click: function(node, e) {
                    if(node.attributes.href) {
                        Ext.History.add(node.attributes.href);
                        node.expand();
                    }
                },
                scope: this
            }
        });

        Portal.panels.MenuContainer.superclass.constructor.call(this, config);

        this.getSelectionModel().on('beforeselect', function(sm, newNode) {
            return newNode.attributes.allowSelect !== false;
        });
    }
});

Ext.reg('menucontainer', Portal.panels.MenuContainer);