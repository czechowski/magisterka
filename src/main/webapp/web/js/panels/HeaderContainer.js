Ext.ns('Portal.panels');

Portal.panels.HeaderContainer = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        var tpl = new Ext.XTemplate('<a href="#user?id={id}">{firstName} {lastName}</a>');

        config = Ext.apply({
            layout: 'hbox',
            height: 25,
            id: 'headercontainer',
            layoutConfig: {
                align: 'center'
            },
            items: [{
                xtype: 'box',
                itemId: 'userInfo',
                html: tpl.apply(config.userData),
                width: 200,
                margins: '0 5 0 0',
                tpl: tpl,
                cls: 'header-user-data'
            },{
                xtype: 'searchfield',
                width: 250,
                emptyText: 'Szukaj'
            },{
                xtype: 'container',
                flex: 1,
                items: [{
                    xtype: 'button',
                    text: 'Wyloguj',
                    handler: function() {
                        Portal.common.Auth.logout();
                    },
                    iconCls: 'header-logout-icon',
                    pack: 'end',
                    style: 'position:absolute;right:0;',
                    width: 100
                }]
            }]
        }, config);

        Portal.panels.HeaderContainer.superclass.constructor.call(this, config);
    },
    setUserInfo: function(userData) {
        var cmp = this.getComponent('userInfo');
        cmp.update(cmp.tpl.apply(userData));
    }
});

Ext.reg('headercontainer', Portal.panels.HeaderContainer);