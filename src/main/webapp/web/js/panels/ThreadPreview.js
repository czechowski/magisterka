Ext.ns('Portal.panels');

Portal.panels.ThreadPreview = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        this.store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/forum',
            fields: ['id', 'subject', 'text', 'user', 'replies', 'parent', {
                name: 'date',
                type: 'date',
                format: 'c'
            }],
            restful: true,
            writer: new Portal.common.XJsonWriter(),
            autoLoad: false
        });

        var tpl = new Ext.XTemplate('<div class="thread-preview-subject">{subject}</div>',
            '<table class="thread-preview-table">',
            '<tpl for="data">',
            '<tr class="thread-preview-item-body">',
            '<td class="thread-preview-item-info"><div class="thread-preview-item-user">{user}</div><div class="thread-preview-item-date">{date}</div></td>',
            '<td class="thread-preview-item-text">{text}</td>',
            '</tr></tpl>',
            '<table>');

        this.dataView = new Ext.DataView({
            store: this.store,
            autoHeight:true,
            tpl: tpl,
            itemSelector: 'tr.thread-preview-item-body',
            prepareData: function(data, recordIndex, record) {
                var user = '';
                if(data.user) {
                    user = Portal.common.Util.prepareObjectLink('user', record.data.user.id, record.data.user.label);
                } else {
                    user = Portal.common.Util.prepareObjectLink('user', Portal.common.Auth.getUserData('id'), Portal.common.Auth.getUserData('label'));
                }
                return {
                    date: Ext.isDate(data.date) ? data.date.format('d M Y H:i') : new Date().format('d M Y H:i'),
                    user: user,
                    text: data.text
                }
            },
            collectData : function(records, startIndex){
                var data = [], i = 0, len = records.length, subject = '';
                for(; i < len; i++){
                    if(i === 0) {
                        subject = records[0].data.subject;
                    }
                    data[data.length] = this.prepareData(records[i].data, startIndex + i, records[i]);
                }
                return {
                    data: data,
                    subject: subject
                }
            }
        });

        config = Ext.apply({
            items: [this.dataView],
            cls: 'thread-preview',
            frame: false,
            layout: 'fit',
            autoScroll: true,
            padding: '0'
        }, config);

        Portal.panels.ThreadPreview.superclass.constructor.call(this, config);

        this.on('afterrender', function() {
            this.loadMask = new Ext.LoadMask(this.bwrap, {
                store: this.store
            });
        }, this);
    },
    setThreadUrl: function(parentId) {
        var url = 'svc/forum/' + parentId + '/thread';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        this.store.reload();
    }
});