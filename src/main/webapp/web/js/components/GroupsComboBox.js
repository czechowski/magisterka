Ext.ns('Portal.components');

Portal.components.GroupsComboBox = Ext.extend(Ext.form.ComboBox, {
    constructor: function(config) {
        config = config || {};

        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/courses/' + config.courseId + '/groups',
            fields: ['id', 'name', 'year'],
            restful: true,
            autoLoad: false
        });

        var tpl = '<tpl for="."><div class="x-combo-list-item"><span class="groups-renderer-name">{name}</span><span class="groups-renderer-year">{year}</span></div></tpl>';

        config = Ext.apply({
            store: store,
            displayField: 'name',
            valueField: 'id',
            triggerAction: 'all',
            pageSize:10,
            tpl: tpl,
            minChars: 999
        }, config);

        Portal.components.GroupsComboBox.superclass.constructor.call(this, config);
    },
    setCourseId: function(courseId, clear) {
        var url = 'svc/courses/' + courseId + '/groups';
        this.store.proxy.setApi({
            read    : url,
            create  : url,
            update  : url,
            destroy : url
        });
        if(clear === true) {
            this.reset();
            this.store.reload();
        }
    }
});

Ext.reg('groupscombobox', Portal.components.GroupsComboBox);