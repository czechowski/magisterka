Ext.ns('Portal.components');

Portal.components.ContactsComboBox = Ext.extend(Ext.form.ComboBox, {
    constructor: function(config) {

        var store = new Ext.data.JsonStore({
            root: 'data',
            url: 'svc/contacts',
            fields: ['id', 'label', 'email', 'firstName', 'lastName'],
            restful: true,
            autoLoad: false
        });

        var tpl = '<tpl for="."><div class="x-combo-list-item"><div class="contacts-combo-label">{label}</div><div class="contacts-combo-email">{email}</div></div></tpl>';

        config = Ext.apply({
            store: store,
            displayField: 'email',
            valueField: 'email',
            triggerAction: 'all',
            pageSize:10,
            vtype: 'email',
            tpl: tpl,
            minChars: 999
        }, config);
        
        Portal.components.ContactsComboBox.superclass.constructor.call(this, config);
    }
});

Ext.reg('contactscombobox', Portal.components.ContactsComboBox);