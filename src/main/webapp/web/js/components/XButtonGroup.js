Ext.ns('Portal.components');

Portal.components.XButtonGroup = Ext.extend(Ext.ButtonGroup, {
    monitorResize: true,
    initComponent: function() {
        var that = this;
        var onShowOrHide = function(owner) {
            that.layout.layout();
        }

        this.on('add', function(group, cmp) {
            cmp.on({
                show: onShowOrHide,
                hide: onShowOrHide
            });
        });

        Portal.components.XButtonGroup.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('xbuttongroup', Portal.components.XButtonGroup);