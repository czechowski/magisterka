Ext.ns('Portal.common.Auth');

Portal.common.Auth = new (Ext.extend(Ext.util.Observable, {
    constructor: function() {
        this.addEvents('loginsuccessful', 'loginfailure', 'registrationsuccessful', 'registrationfailure');

        var config = {
            listeners: {
                loginsuccessful: this.onLoginSuccessful,
                loginfailure: this.onLoginFailure,
                registrationsuccessful: this.onRegistrationSuccessful,
                registrationfailure: this.onRegistrationFailure,
                scope: this
            }
        };

        Ext.apply(this, config);

        Ext.util.Observable.prototype.constructor.call(this, config);
    },
    onLoginSuccessful: function(userData) {
        this.USER_TYPE = userData.type;
        this.USER_DATA = userData;
        Portal.common.App.init({
            userData: userData
        });
    },
    onLoginFailure: function() {
        this.clearAuthorizationCookie();
        Ext.state.Manager.clear("authorization");
    },
    onRegistrationSuccessful: function(userData) {
        var registrationWindow = Ext.WindowMgr.get('portal.windows.registrationwindow');
        if(registrationWindow) {
            registrationWindow.close();
        }
        var welcomeMsg = ['Witaj ', userData.firstName, ' ', userData.lastName, '!<br/>',
        'Rejestracja Twojego konta przebiegła pomyślnie. Zaloguj się, aby skorzystać z aplikacji.'].join('');
        Ext.MessageBox.alert('Rejestracja', welcomeMsg);
    },
    onRegistrationFailure: function(response) {
    },
    login: function(params, remember) {
        var authHash = Ext.isObject(params) ? Ext.util.Base64.encode(params.email + ':' + params.password) : params;
        if(Ext.isObject(Ext.Ajax.defaultHeaders)) {
            delete Ext.Ajax.defaultHeaders.Authorization;
        }
        Ext.Ajax.request({
            url: 'svc/users/current',
            headers : {
                Authorization : "Basic " + authHash
            },
            method: 'GET',
            scope: this,
            success: function(response) {
                Ext.Ajax.defaultHeaders = {
                    Authorization: "Basic " + authHash
                };
                if(remember === true) {
                    Ext.state.Manager.set("authorization", authHash);
                }
                this.fireEvent('loginsuccessful', Portal.common.Util.getResponseJson(response));
            },
            failure: function() {
                this.fireEvent('loginfailure');
            }
        });
    },
    logout: function() {
        this.clearAuthorizationCookie();
        Ext.state.Manager.clear("authorization");
        delete Ext.Ajax.defaultHeaders.Authorization;
        location.href = location.href.replace(location.hash, '');
    },
    register: function(params) {
        if(Ext.isObject(Ext.Ajax.defaultHeaders)) {
            delete Ext.Ajax.defaultHeaders.Authorization;
        }
        Ext.Ajax.request({
            url: 'svc/users',
            method: 'POST',
            jsonData: params,
            scope: this,
            success: function(response) {
                this.fireEvent('registrationsuccessful', Portal.common.Util.getResponseJson(response));
            },
            failure: function(response) {
                this.fireEvent('registrationfailure', Portal.common.Util.getResponseJson(response));
            }
        });
    },
    isLecturer: function() {
        return this.USER_TYPE === 'LECTURER';
    },
    init: function() {
        if(Ext.state.Manager.get("authorization")) {
            this.login(Ext.state.Manager.get("authorization"));
        } else {
            new Portal.windows.LoginWindow().show();
        }
    },
    getUserData: function(property) {
        return Ext.isObject(this.USER_DATA) ? this.USER_DATA[property] : '';
    },
    setAuthorizationCookie: function() {
        if (Ext.Ajax.defaultHeaders.Authorization) {
            Ext.util.Cookies.set(
                "Authorization",
                Ext.Ajax.defaultHeaders.Authorization.substring(6),
                new Date(new Date().getTime()+(1000*60)));
        }
    },
    clearAuthorizationCookie: function() {
        Ext.util.Cookies.set("Authorization", "", new Date(new Date().getTime()));
    }
}))();
