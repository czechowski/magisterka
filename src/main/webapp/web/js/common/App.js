Ext.ns('Portal.common');

Portal.common.App = {
    viewport: null,
    init: function(config) {
        if(this.viewport) {
            return;
        }
        this.initViewport(config);
        Ext.History.init(Portal.common.NavigationMgr.init, Portal.common.NavigationMgr);
    },
    initViewport: function(config) {
        this.viewport = new Ext.Viewport({
            layout: 'border',
            items: [{
                xtype: 'headercontainer',
                region: 'north',
                frame: false,
                border: false,
                bodyStyle: 'background-color: transparent;',
                margins: '5',
                userData: config.userData
            },{
                xtype: 'menucontainer',
                region: 'west',
                width: 200,
                margins: '0 5 5 5'
            },{
                xtype: 'contentcontainer',
                region: 'center',
                margins: '0 5 5 0'
            }]
        });
    },
    preInit: function() {
        var cp = new Ext.state.CookieProvider({
            secure: true,
            expires: new Date(new Date().getTime()+(1000*60*60*24*30))
        });
        Ext.state.Manager.setProvider(cp);
    }
};