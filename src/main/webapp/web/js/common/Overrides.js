//Ext classes overrides

Ext.override(Ext.form.FormPanel, {
    labelAlign: 'top',
    frame: true,
    monitorValid: true
});

Ext.override(Ext.Window, {
    border: false,
    autoHeight: true
});

Ext.override(Ext.form.HtmlEditor, {
    getErrors: Ext.form.TextField.prototype.getErrors
});

Ext.override(Ext.grid.RowSelectionModel, {
    singleSelect: true
});

Ext.override(Ext.grid.GridPanel, {
    listeners: {
        "click": function(evt) {
            if (evt.getTarget(".x-grid3-row") !== null || evt.getTarget(".x-grid3-hd") !== null || evt.getTarget(".x-grid-editor") !== null) {
                return;
            }
            this.getSelectionModel().clearSelections();
        }
    }
});
