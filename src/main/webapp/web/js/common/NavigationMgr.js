Ext.ns('Portal.common');

Portal.common.NavigationMgr = {
    initialized: false,
    init: function() {
        if(this.initialized) {
            return;
        }

        Ext.History.on('change', this.onHistoryChange, this);
        this.onHistoryChange(Ext.History.getToken());
    },
    onHistoryChange: function(token) {
        var parts = token ? token.split('?') : [];
        var view = parts[0] || '';
        var params = Ext.urlDecode(parts[1]);
        this.getContentContainer().setContent(view, params);
        this.getMenuContainer().selectPath('//#' + view, 'href');
    },
    getContentContainer: function() {
        if(!this.contentContainer) {
            this.contentContainer = Ext.getCmp('contentcontainer');
        }
        return this.contentContainer;
    },
    getMenuContainer: function() {
        if(!this.menuContainer) {
            this.menuContainer = Ext.getCmp('menucontainer');
        }
        return this.menuContainer;
    }
};