Ext.ns('Portal.common');

Portal.common.XJsonWriter = Ext.extend(Ext.data.JsonWriter, {
    encode: false,
    render : function(params, baseParams, data, rs, action) {
        if (this.encode === true) {
            Ext.apply(params, baseParams);
            params[this.meta.root] = Ext.encode(data);
        } else {
            var jdata = Ext.apply({}, data);
            delete jdata.id;
            params.jsonData = jdata;
        }
    }
});
