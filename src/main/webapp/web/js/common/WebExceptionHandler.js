Ext.ns('Portal.common');

Portal.common.WebExceptionHandler = (function() {
    var handler = function(status, responseJson, options) {
        responseJson = responseJson || {};
        var exception =  responseJson.exception;
        var message = responseJson.message;
        var title = "Błąd";

        switch(status) {
            case 401:
                title = 'Nieautoryzowany dostęp';
                if(options.url === 'svc/users/current') {
                    message = 'Podany e-mail bądź hasło są nieprawidłowe';
                } else {
                    message = 'Nie masz uprawnień do tego obiektu'
                }
                break;
            case 409:
                title = 'Wystąpił konflikt';
                if(exception === 'DuplicatedEmailException') {
                    message = 'Podany e-mail istnieje już w systemie';
                }
                break;
        }

        if(message) {
            Ext.MessageBox.alert(title, message).setIcon(Ext.MessageBox.ERROR);
        }
    }

    Ext.Ajax.on('requestexception', function(conn, response, options) {
        handler(response.status, Portal.common.Util.getResponseJson(response), options);
    });

    return {
        exceptionHandler: handler
    };
})();