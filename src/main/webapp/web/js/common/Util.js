Ext.ns('Portal.common');

Ext.Ajax.disableCaching = Ext.isIE;

Portal.common.Util = {
    getResponseJson: function(response) {
        try {
            return Ext.decode(response.responseText);
        } catch(e) {
            return {};
        }
    },
    prepareObjectLink: function(view, id, label) {
        return ['<a href="#', view, '?id=', id, '">', label, '</a>'].join('');
    }
};